package edu.unibo.cs.STDLock;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

import edu.unibo.cs.STDLock.LAM.node.TId;

/** Context for variables substitutions
 *
 * Used to get name of variables in a consistent way. For now,
 * they are limited to x, y, z, w, with integers appended if
 * a number great of 4 is requested.
 *
 * @author Natale Patriciello
 *
 */
public class StringContext {

	private Map<String, Integer> names;
	private final List<String> initialNames;
	private final List<String> freshNames;
    private Pattern pattern = Pattern.compile("([a-zA-z]+)([0-9]*)");

	/** Create the context
	 *
	 * If you have already used some names, please feed the generator
	 * with a call to pushInitialNames.
	 *
	 */
	public StringContext()
	{
		initialNames = new ArrayList<String>();
		freshNames = new ArrayList<String>();

		names = new HashMap<String, Integer>();
		names.put("x", 0);
	}

	/**
	 * Get the initial names; if count <= 4, the names returned are
	 * x, y, z and w. Otherwise, x, y, z and w still be used, but in conjuction
	 * with numbers (e.g. x1, y1, z1, w1..)
	 *
	 * @param count Count of names that should be returned
	 * @return List of names
	 */
	public List<String> getInitialNames(int count)
	{
		names = new HashMap<String, Integer>();
		names.put("x", 0);

		List<String> names = makeNames(count);

		initialNames.addAll(names);

		return names;
	}

	/**
	 * Push names into the generator. Names accepted are in the form
	 * [x|y|z|w][0-9]*.
	 *
	 * @param var List of names to feed the generator
	 */
	public void pushNames(Collection<String> var)
	{
		for (String name : var) {
            Matcher matcher = pattern.matcher(name);
            matcher.find();

            String w = matcher.group(1);
            int i, used = 0;
            try {
                i = Integer.parseInt(matcher.group(2));
            } catch (Exception e) {
                i = -1;
            }

            try {
                used = names.get(w);
            } catch (Exception e) {
                names.put(w, 0);
                used = 0;
			}

            if (i > used)
                used = i;

            ++used;
            names.put(w, used);
		}
	}

	public void pushNames(Set<TId> newNames)
	{
        List<String> tmp = new ArrayList<String>();
		for (TId name : newNames) {
			String id = name.toString().replaceAll("\\s","");
            tmp.add(id);
		}

        pushNames(tmp);
	}

	/**
	 * Get fresh names
	 *
	 * @param count Number of fresh names to return
	 * @return A list of fresh names
	 */
	public List<String> getFreshNames(int count)
	{
		List<String> names = makeNames(count);

		freshNames.addAll(names);
		return names;
	}

	private List<String> makeNames(int count)
	{
		List<String> ret = new ArrayList<String>();
		String key = "x";

		for (int i=0; i<count; i++) {

			int used = names.get(key);
			++used;

			names.put(key, used);

			if (used > 1)
				ret.add(key+(used-1));
			else
				ret.add(key);
		}

		return ret;
	}

	public Mutation compareValues(List<String> end)
	{
		return null;
	}

	public void pushNames(String[] n) {
        List<String> tmp = new ArrayList<String>();

		for (String name : n) {
			String id = name.toString().replaceAll("\\s","");
            tmp.add(id);
		}

        pushNames(tmp);
	}

}

