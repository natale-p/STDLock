package edu.unibo.cs.STDLock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import edu.unibo.cs.STDLock.LAM.node.AFunctionExpr;

/**
 * Represent a Partition.
 *
 * A partition is a level on the tree formed by Method. A fundamental law
 * is that no method on lower level should call a method on above levels;
 * The top level is the level which have the root Method on the three.
 *
 * @author danimoth
 *
 */

public class Partition<E> extends ArrayList<E> implements Set<E> {

	private static final long serialVersionUID = -8630678101722260965L;
	private static Logger logger=Logger.getLogger("edu.unibo.cs.STDLock.Partition");
	private int order;

	private final Map<Method, Method> cTree;
	/**
	 * Create a Partition from another collection.
	 *
	 * @param arg the collection which forms the partition
	 */
	public Partition(List<E> arg)
	{
		super(arg);
		order = -1;

		cTree = new HashMap<Method, Method>();
	}

	/**
	 * Get the order of the Partition
	 *
	 * Each member of the Partition has a Mutation, which has an order.
	 * So, the order of the Partition is the lcm of the orders of its members.
	 *
	 * @return Order of the partition
	 */
	public int getOrder() {
		if (size() == 0) {
			return 0;
		}

		if (order >= 0)
			return order;

		long[] orders = new long[size()];

		for (int j=0; j<size(); j++) {
			Method first = (Method) get(j);

			IntegerContext ctx = new IntegerContext();

			// Contiene i veri argomenti che passano di metodo in metodo.
			List<Integer> realArgsOfNextMethod = ctx.getInitialValues(first.getArgsCount());
            //logger.fine("First arg for " + first + " = " + realArgsOfNextMethod) ;

			// Contiene un map tra i valori formali e quelli in input, aggiornato
			// ad ogni iterazione
			Map<String, Integer> formalToArgs = new HashMap<String, Integer>();

			int count = 0;
			for (String bound : first.getSignature().split("\\)")[0].split("\\(")[1].split(",")) {
				formalToArgs.put(bound, realArgsOfNextMethod.get(count));
				++count;
			}

            //System.out.println("bound --> arg: " + formalToArgs);

			for (int i=j; i<size()+j; i++) {
				Method A = (Method) get(i%size());
				Method B = (Method) get((i+1)%size());

				// Ho le variabili con cui A chiama B, ma sono ancora quelle "formali"
				// non quelle vere che sto usando dall'inizio
				List<String> instantiationVar = A.getOnlyInstantiationVariables(B);

                //System.out.println(A + " chiama " + B + " con " + instantiationVar);


				// Cosa passo realmente al metodo successivo? Vedo cosa ho
				// nel map che mi son portato dietro dalla scorsa iterazione.
				realArgsOfNextMethod = new ArrayList<Integer>();

				for (String value : instantiationVar) {
					if (formalToArgs.containsKey(value)) {
						realArgsOfNextMethod.add(formalToArgs.get(value));
					} else {
						realArgsOfNextMethod.add(ctx.getFreshValues(1).get(0));
					}
				}

				// Devo aggiornare il map per il metodo successivo, con quelle che realmente
				// gli passo.
				formalToArgs.clear();
				count = 0;
				if (B.getSignature().split("\\)")[0].split("\\(")[1].split(",").length != realArgsOfNextMethod.size()) {
                    System.err.println("Syntax error: Method " + A + " calls Method " + B + " with wrong number of args. Exiting");
                    System.exit(1);
                }

				for (String bound : B.getSignature().split("\\)")[0].split("\\(")[1].split(",")) {
					formalToArgs.put(bound, realArgsOfNextMethod.get(count));
					++count;
				}
			}

			Mutation mut = ctx.compareValues(realArgsOfNextMethod);

			orders[j] = mut.getOrder();
			logger.fine("Per il metodo " + get(j) + " la mutazione è " + mut + " e l'ordine è " + orders[j]);
		}

		long max_order = 0;
		for (long order : orders) {
			if (order > max_order)
				max_order = order;
		}

		logger.fine("La partizione " + this + " ha ordine " + max_order);
		order = (int) max_order;
		return order;
	}


	public Method getEP(Method N)
	{
		logger.fine("@@Voglio EP da " + N);
		int indexOfN = indexOf(N);
		assert(indexOfN >= 0);

		if (cTree.containsKey(N)) {
			return new Method(cTree.get(N));
		}

		Method EP_N = new Method (N);

		StringContext ctx = new StringContext();
		EP_N.initStringContext(ctx);

		for (int i=indexOfN; i<size()-1; i++) {
			int nextId = i+1;
			Method child = new Method((Method) get(nextId));

			ASTDepthInlinePrinter pr = new ASTDepthInlinePrinter();
			EP_N.getAST().apply(pr);

			int countChildrenBefore = EP_N.getChildren().size();
			logger.fine("===>Sono " + EP_N +"=" + pr.getPrinted()+ " e mergio " + child);

			Pair<AFunctionExpr, Method> firstInvkOfChild = EP_N.getFirstInvocationOf(child);

			EP_N.merge(firstInvkOfChild.getLeft(), child, ctx);
			EP_N.mergeChildrenOf(firstInvkOfChild, child);

			assert(countChildrenBefore + child.getChildren().size() == EP_N.getChildren().size()+1);
		}


		boolean right_child = false;
		Method first = (Method) get(0);

		for (Pair<AFunctionExpr, Method> t : EP_N.getChildren()) {
			if (t.getRight().equals(first))
				right_child = true;
		}

		assert(right_child);

		cTree.put(N, EP_N);
		return new Method(EP_N);
	}
}
