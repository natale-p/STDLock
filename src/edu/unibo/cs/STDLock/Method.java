package edu.unibo.cs.STDLock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.unibo.cs.STDLock.LAM.analysis.DepthFirstAdapter;
import edu.unibo.cs.STDLock.LAM.node.AFunctionExpr;
import edu.unibo.cs.STDLock.LAM.node.AParallelExpr;
import edu.unibo.cs.STDLock.LAM.node.AParenthesisExpr;
import edu.unibo.cs.STDLock.LAM.node.AZeroExpr;
import edu.unibo.cs.STDLock.LAM.node.Node;
import edu.unibo.cs.STDLock.LAM.node.PExpr;
import edu.unibo.cs.STDLock.LAM.node.TId;
import edu.unibo.cs.STDLock.LAM.node.TUid;
import edu.unibo.cs.STDLock.LAM.node.TZero;

/** Method
 *
 * A method is defined like:
 * <pre>
 * {@code
 * M(x)=N(x,y,z)
 * N(x,y,z)=(x,y)||M(z)
 * }
 * </pre>
 *
 * M(x) and N(x,y,z) are their signature, and everything after the "=" is their body.
 * Every method has one or more childs (N is a child of M) and one or more fathers
 * (N is a father of M). Recursion allow these strange things :)
 *
 *
 * @author Natale Patriciello
 *
 */
public class Method extends MethodCore {

	private static Logger logger=Logger.getLogger("edu.unibo.cs.STDLock.Method");

	public Method(String name, String definition) throws Exception
	{
		super(name, definition);
	}

	public Method(Method other)
	{
		super(other);
	}

	/**
	 *
	 * Method m = new Method("M", "M(x,y)=N(x,y)");
	 * Method n = new Method("N", "N(a,b)=M(c,d)");
	 *
	 * Map<String, String> nToM = m.getInstantiationVariables(n);
	 *
	 * nToM = { "a" => "x" , "b" => "y" }
	 *
	 * @param M
	 * @param N
	 * @return
	 */
	public Map<String, String> getInstantiationVariables(AFunctionExpr NInvk, Method N)
	{
		Map<String, String> map = new HashMap<String, String>();
		assert(NInvk != null);

		List<String> paramOfN = getOnlyInstantiationVariables(NInvk);

		String formalOfN[] = N.getSignature().split("\\)")[0].split("\\(")[1].split(",");

		assert(paramOfN.size() == formalOfN.length);

		for (int i=0; i<formalOfN.length; i++) {
			map.put(formalOfN[i], paramOfN.get(i).toString().replaceAll("\\s", ""));
		}

		return map;
	}

	public List<String> getOnlyInstantiationVariables(AFunctionExpr NInvk)
	{
		List<TId> paramOfN = NInvk.getArgs();
		List<String> ret = new ArrayList<String>();

		for (TId arg : paramOfN) {
			ret.add(arg.toString().replaceAll("\\s", ""));
		}

		return ret;
	}

	public List<String> getOnlyInstantiationVariables(Method N)
	{
		AFunctionExpr point = getFirstInvocationOf(N).getLeft();

		return getOnlyInstantiationVariables(point);
	}

	private void instantiateBound(Map<String, String> myFormalToReal)
	{
		for (TId var : boundVariables) {
			String id = var.toString().replaceAll("\\s","");

			String newName = myFormalToReal.get(id);
			assert(newName != null);

			var.setText(newName);
		}
	}

	private void instantiateFree(StringContext ctx)
	{
		Map<String, String> alreadyObtained = new HashMap<String, String>();

		for (TId var : freeVariables) {
			String id = var.toString().replaceAll("\\s", "");
			if (alreadyObtained.containsKey(id)) {
				var.setText(alreadyObtained.get(id));
			} else {
				String newName = ctx.getFreshNames(1).get(0);
				alreadyObtained.put(id, newName);
				var.setText(newName);
			}
		}
	}

	public void instantiate (Map<String, String> myFormalToReal, StringContext ctx)
	{
		instantiateFree(ctx);
		instantiateBound(myFormalToReal);
	}

	public void removeChild(Pair<AFunctionExpr, Method> child)
	{
		children.remove(child);
	}

	public void mergeChildrenOf(Pair<AFunctionExpr, Method> invkPointOfN, Method N)
	{
		List<Pair<AFunctionExpr, Method>> a = new ArrayList<Pair<AFunctionExpr, Method>>();
		List<Pair<AFunctionExpr, Method>> childrenOfN = N.getChildren();

		for (int i=0; i<children.indexOf(invkPointOfN); i++) {
			a.add(children.get(i));
		}

		for (int i=0; i<childrenOfN.size(); i++) {
			a.add(childrenOfN.get(i));
		}

		for (int i=children.indexOf(invkPointOfN)+2; i<children.size(); i++) {
			a.add(children.get(i));
		}

		children = a;
	}

	/** @brief Merge N, called at invkPointOfN, into this
	 *
	 * NOTE: This destroy the AST of N, but not his child list.
	 *
	 * @param invkPointOfN
	 * @param N
	 * @param ctx
	 */
	public void merge (AFunctionExpr invkPointOfN, Method N, StringContext ctx)
	{
		nameMerge += " + " + N.getNameMerge();

		ASTDepthInlinePrinter pr = new ASTDepthInlinePrinter();
		N.getAST().apply(pr);
		logger.fine("Sono " + this.ASTToString());
		logger.fine("Mergio nel punto " + invkPointOfN + " il met "+ N+ "="+ pr.getPrinted());

		Map<String, String> instantiationVar = getInstantiationVariables(invkPointOfN, N);

		N.instantiate(instantiationVar, ctx);

		pr = new ASTDepthInlinePrinter();
		N.getAST().apply(pr);
		logger.fine("Instanziato, il met è " + pr.getPrinted());
		logger.fine("Con bound " + N.getBound() + " e free " + N.getFree());

		AParenthesisExpr x = new AParenthesisExpr(N.getAST().getPExpr());

		invkPointOfN.replaceBy(x);

		// Se ho duplicato delle variabili bound me le devo aggiungere ai miei bound

		/*Set<TId> toAdd = new HashSet<TId>();
		for (TId NBound : N.getBound()) {
			String NBoundText = NBound.toString().replaceAll("\\s", "");

			for (TId myBound : boundVariables) {
				String myBoundText = myBound.toString().replaceAll("\\s", "");

				if (NBoundText.equals(myBoundText))
					toAdd.add(NBound);
			}
		}

		boundVariables.addAll(toAdd);

		int siz = freeVariables.size();
		freeVariables.addAll(N.getFree());

		assert(siz+N.getFree().size() == freeVariables.size());*/

		// TODO Questo coso rivà dentro all'albero. In realtà (vedi codice che
		// è commentato) si potrebbe "mergiare" le free e le bound, stando attenti
		// alle bound perchè potrebbero essere duplicati. In ogni caso, il codice
		// sopra è buggato, non so perchè. Lo si vede in MethodTreeTest (caso non
		// lineare) in cui viene indicato un bug inesistente.
		initVar();

		definition = getSignature() + "=" + ASTToString();

		pr = new ASTDepthInlinePrinter();
		ast.apply(pr);
		logger.fine("E io istanziato divento " + pr.getPrinted());
		logger.fine("Con bound " + getBound() + " e free " + getFree());
	}

	public void removeCallTo(Pair<AFunctionExpr, Method> pointOfInvk)
	{
		assert(pointOfInvk != null);
		AFunctionExpr NInvk = pointOfInvk.getLeft();

		for (TId arg : NInvk.getArgs()) {
			//assert(boundVariables.contains(arg) || freeVariables.contains(arg));
			boundVariables.remove(arg);
			freeVariables.remove(arg);
		}

		children.remove(children.indexOf(pointOfInvk));

		assert(NInvk != null);

		AZeroExpr zero = new AZeroExpr(new TZero());

		NInvk.replaceBy(zero);
	}


	public Method hasTwoOrMoreSameChild()
	{
		Set<Method> set = new HashSet<Method>();

		for (Pair<AFunctionExpr, Method> p: children) {
			Method N = p.getRight();

			if (! (this.equals(N) || set.add(N))) {
				return N;
			}
		}

		return null;
	}


	public boolean hasTwoOrMoreSelfChild()
	{
		Set<String> set = new HashSet<String>();

		for (Pair<AFunctionExpr, Method> p: children) {
			Method N = p.getRight();

			if (this.equals(N) && (! set.add(N.getName()))) {
				return true;
			}
		}

		return false;
	}

	public Method doThirdTransformation(Set<Method> allMethods)
	{

		/*
		 * Io sono M!
		 *
		 * n = numero di self invocazioni;
		 *
		 * Costruisci il body di MAUX
		 * Costruisci la signature di MAUX
		 * Crea MAux e inseriscilo nei metodi
		 *
		 * M chiama MAUX
		 * M si ricostruisce l'AST
		 */

		int numberOfSelfInvk = 0;
		List<AFunctionExpr> selfInvks = new ArrayList<AFunctionExpr>();
		ArrayList<LinkedList<TId>> paramsOfSelfInvks = new ArrayList<LinkedList<TId>>();

		for (Pair<AFunctionExpr, Method> p : children) {
			Method N = p.getRight();

			if (N.equals(this)) {
				++numberOfSelfInvk;
				selfInvks.add(p.getLeft());

				paramsOfSelfInvks.add(p.getLeft().getArgs());
			}
		}

		Pattern namePatt = Pattern.compile("([a-zA-Z]+)([0-9]*)");
		Matcher m = namePatt.matcher(getName());
		String auxName = null;
		if (m.matches()) {
			auxName = m.group(1) + "AUX" + m.group(2);
		} else {
			assert(1==0);
		}

		StringContext ctx = new StringContext();
		List<String> auxFormal = ctx.getInitialNames(argsCount*numberOfSelfInvk);

		// Per ogni chiamata metto il flattening, sostituendo opportunamente le variabili
		// legate.

		Node flattening = getFlattening(selfInvks);

		List<PExpr> flatteningInstance = new ArrayList<PExpr>();
		String[] formals = getSignature().split("\\)")[0].split("\\(")[1].split(",");
		Map<String, String> freeToAuxFree = new HashMap<String, String>();
		List<TId> auxCallParameters = new ArrayList<TId>();

		assert(formals.length == argsCount);
		assert(formals.length*numberOfSelfInvk == auxFormal.size());

		for (int i=0; i<numberOfSelfInvk; i++) {
			Map<String, String> formalToAuxFormal = new HashMap<String, String>();
			for (int j=0; j<argsCount; j++) {
				formalToAuxFormal.put(formals[j], auxFormal.get(i*argsCount+j));
			}

			PExpr instancedFlattening = (PExpr) flattening.clone();

			ASTDepthWalkerForVariables walker = new ASTDepthWalkerForVariables();
			instancedFlattening.apply(walker);

			for (TId var : walker.variables) {
				String name = var.getText();
				if (formalToAuxFormal.containsKey(name)) {
					var.setText(formalToAuxFormal.get(name));
				} else if (freeToAuxFree.containsKey(name)) {
					String free = freeToAuxFree.get(name);
					assert(free != null);
					var.setText(free);
				} else {
					String free = new String(ctx.getFreshNames(1).get(0));
					var.setText(free);

					freeToAuxFree.put(name, free);
				}
			}

			flatteningInstance.add(instancedFlattening);

			LinkedList<TId> paramOfSelfInvk = paramsOfSelfInvks.get(i);

			for (TId id : paramOfSelfInvk) {
				String name = id.getText().replaceAll("\\s","");
				TId newParam = new TId("");

				if (formalToAuxFormal.containsKey(name)) {
					newParam.setText(formalToAuxFormal.get(name));
				} else if (freeToAuxFree.containsKey(name)) {
					String free = freeToAuxFree.get(name);
					newParam.setText(free);
				} else {
					String free = ctx.getFreshNames(1).get(0);
					newParam.setText(free);

					freeToAuxFree.put(name, free);
				}

				auxCallParameters.add(newParam);
			}
		}

		AFunctionExpr auxCall = new AFunctionExpr();
		auxCall.setName(new TUid(auxName));
		auxCall.setArgs(auxCallParameters);

		boolean auxAdded = false;

		if (flatteningInstance.size() % 2 != 0) {
			flatteningInstance.add(auxCall);
			auxAdded = true;
		}

		int round = (int) Math.round(Math.log(flatteningInstance.size())/Math.log(2));

		for (int i=0; i<round; i++) {
			List<PExpr> temp = new ArrayList<PExpr>();
			for (int j=0; j<flatteningInstance.size()-1; j+=2) {
				AParallelExpr parallel = new AParallelExpr();
				parallel.setLeft(flatteningInstance.get(j));
				parallel.setRight(flatteningInstance.get(j+1));
				temp.add(parallel);
			}

			flatteningInstance = temp;
		}

		assert(flatteningInstance.size() == 1);
		Node auxAST = null;

		if (auxAdded == false) {
			AParallelExpr parallel = new AParallelExpr();
			parallel.setLeft(flatteningInstance.get(0));
			parallel.setRight(auxCall);

			auxAST = parallel;
		} else {
			auxAST = flatteningInstance.get(0);
		}

		Method MAux = null;
		ASTDepthInlinePrinter printer = new ASTDepthInlinePrinter();
		auxAST.apply(printer);

		try {
			String def = getAuxSignature(auxName, numberOfSelfInvk, auxFormal)+printer.getPrinted();
			MAux = new Method(auxName, def);
			allMethods.add(MAux);
			MAux.rebuildChildrenList(allMethods);
		} catch (Exception e) {
			System.out.println(e);
			assert(1 == 0);
		}

		AFunctionExpr callToAux = new AFunctionExpr();
		callToAux.setName(new TUid(auxName));

		LinkedList<TId> paramToAux = new LinkedList<TId>();

		for (int i=0; i<numberOfSelfInvk; i++) {
			for (int j=0; j<argsCount; j++) {
				paramToAux.add(new TId(formals[j]));
			}
		}

		callToAux.setArgs(paramToAux);

		printer = new ASTDepthInlinePrinter();
		callToAux.apply(printer);

		this.definition = this.getSignature() + "=" + printer.getPrinted();

		try {
			initAST();
		} catch (Exception e) {
			System.out.println(e);
			assert(0==1);
		}

		rebuildChildrenList(allMethods);

		return MAux;
	}

	private String getAuxSignature(String auxName, int numberOfSelfInvk, List<String> params)
	{
		String auxSignature = auxName + "(";

		for (String param : params) {
			auxSignature += param + ",";
		}

		auxSignature = auxSignature.substring(0, auxSignature.length()-1) + ")=";
		return auxSignature;
	}

	private class ASTRemoveSelfInvk extends DepthFirstAdapter {
		private final List<AFunctionExpr> selfInvk;
		public ASTRemoveSelfInvk(List<AFunctionExpr> selfInvk)
		{
			this.selfInvk = selfInvk;
		}

		@Override
		public void inAFunctionExpr(AFunctionExpr node)
		{
			for (AFunctionExpr invk : selfInvk) {
				if (node.getName().getText().equals(invk.getName().getText())) {
					AZeroExpr zero = new AZeroExpr(new TZero());
					node.replaceBy(zero);
					break;
				}
			}
		}
	}

	public PExpr getFlattening(List<AFunctionExpr> selfInvk)
	{
		PExpr node = (PExpr) ast.getPExpr().clone();

		ASTRemoveSelfInvk walker = new ASTRemoveSelfInvk(selfInvk);
		node.apply(walker);

		return node;
	}
}
