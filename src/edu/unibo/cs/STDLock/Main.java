package edu.unibo.cs.STDLock;

import java.util.HashSet;
import java.util.Set;

public final class Main {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		System.out.println("Esempio ONE");
		esempioElenaUno();
		System.out.println("Esempio ONEBIS");
		esempioElenaUnoBis();
		System.out.println("Esempio DUE");
		esempioElenaDue();
		System.out.println("Esempio PAPERO");
		esempioPapero();
	}

	private static void esempioElenaUno() throws Exception {

		Method A = new Method("A", "A(x)=B(x,y)");
		Method B = new Method("B", "B(x,y)=P(x,y,z)");
		Method P = new Method("P", "P(x,y,z)=(z,y)||M(x,y,z)");
		Method M = new Method("M", "M(x,y,z)=(x,y)||M(y,z,w)");

		Set<Method> allMethods = new HashSet<Method>();

		allMethods.add(A);
		allMethods.add(B);
		allMethods.add(P);
		allMethods.add(M);

		A.rebuildChildrenList(allMethods);
		B.rebuildChildrenList(allMethods);
		P.rebuildChildrenList(allMethods);
		M.rebuildChildrenList(allMethods);

		MethodTree MT = new MethodTree(A);
		Set<Partition<Method>> partitions = MT.getPartitions();
		SaturateMethod worker = new SaturateMethod();

		Method ASaturato = worker.saturate(P, partitions, allMethods);

		CircularitiesFinder cf = new CircularitiesFinder(ASaturato.getAST());

		if (cf.hasCirc()) {
			System.out.println("### C'è dead in " + cf.whoCaused);
		} else {
			System.out.println("### Non c'è dead");
		}
	}

	private static void esempioElenaUnoBis() throws Exception {

		Method A = new Method("A", "A(x)=B(x,y)");
		Method B = new Method("B", "B(x,y)=P(x,y,z)");
		Method P = new Method("P", "P(x,y,z)=M(x,y,z)||N(x,y,z)");
		Method M = new Method("M", "M(x,y,z)=(x,y)||M(y,z,w)");
		Method N = new Method("N", "N(x,y,z)=(x,y)||N(z,y,w)");

		Set<Method> allMethods = new HashSet<Method>();

		allMethods.add(A);
		allMethods.add(B);
		allMethods.add(P);
		allMethods.add(M);
		allMethods.add(N);

		A.rebuildChildrenList(allMethods);
		B.rebuildChildrenList(allMethods);
		P.rebuildChildrenList(allMethods);
		M.rebuildChildrenList(allMethods);
		N.rebuildChildrenList(allMethods);

		MethodTree MT = new MethodTree(A);
		Set<Partition<Method>> partitions = MT.getPartitions();

		SaturateMethod worker = new SaturateMethod();
		Method ASaturato = worker.saturate(A, partitions, allMethods);

		CircularitiesFinder cf = new CircularitiesFinder(ASaturato.getAST());

		if (cf.hasCirc()) {
			System.out.println("### C'è dead in " + cf.whoCaused);
		} else {
			System.out.println("### Non c'è dead");
		}
	}

	private static void esempioElenaDue() throws Exception {

		Method Q = new Method("Q", "Q(x0,x1,x2,x3,x4)=(x3,x1);(x6,x5)||(x0,x6)||M(x1,x2,x0,x4,x5,x6)");
		Method M = new Method("M", "M(x0,x1,x2,x3,x4,x5)=Q(x1,x2,x0,x4,x5)");

		Set<Method> allMethods = new HashSet<Method>();

		allMethods.add(M);
		allMethods.add(Q);

		M.rebuildChildrenList(allMethods);
		Q.rebuildChildrenList(allMethods);

		MethodTree MT = new MethodTree(Q);
		Set<Partition<Method>> partitions = MT.getPartitions();

		SaturateMethod worker = new SaturateMethod();
		Method ASaturato = worker.saturate(Q, partitions, allMethods);

		CircularitiesFinder cf = new CircularitiesFinder(ASaturato.getAST());

		if (cf.hasCirc()) {
			System.out.println("### C'è dead in " + cf.whoCaused);
		} else {
			System.out.println("### Non c'è dead");
		}
	}

	private static void esempioPapero() throws Exception
	{
		Method G = new Method("G", "G(x0,x1,x2,x3,x4,x5,x6)=(x3,x1)||(x0,x8)||(x8,x7)||G(x2,x0,x1,x5,x6,x7,x8)");

		Set<Method> allMethods = new HashSet<Method>();

		allMethods.add(G);

		G.rebuildChildrenList(allMethods);

		MethodTree MT = new MethodTree(G);
		Set<Partition<Method>> partitions = MT.getPartitions();

		SaturateMethod worker = new SaturateMethod();
		Method ASaturato = worker.saturate(G, partitions, allMethods);

		CircularitiesFinder cf = new CircularitiesFinder(ASaturato.getAST());

		if (cf.hasCirc()) {
			System.out.println("### C'è dead in " + cf.whoCaused);
		} else {
			System.out.println("### Non c'è dead");
		}
	}

}
