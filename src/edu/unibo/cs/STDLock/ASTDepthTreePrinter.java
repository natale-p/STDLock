package edu.unibo.cs.STDLock;

import edu.unibo.cs.STDLock.LAM.node.AFunctionExpr;
import edu.unibo.cs.STDLock.LAM.node.APairExpr;
import edu.unibo.cs.STDLock.LAM.node.AParallelExpr;
import edu.unibo.cs.STDLock.LAM.node.AParenthesisExpr;
import edu.unibo.cs.STDLock.LAM.node.ASemicolonExpr;
import edu.unibo.cs.STDLock.LAM.node.AZeroExpr;
import edu.unibo.cs.STDLock.LAM.node.TId;

public class ASTDepthTreePrinter extends ASTDepthInlinePrinter {

	private String spaces;

	public ASTDepthTreePrinter()
	{
		spaces = new String("");
	}

    @Override
    public void caseASemicolonExpr(ASemicolonExpr node)
    {
    	output += spaces + ";\n";
    	spaces += "    ";
        inASemicolonExpr(node);
        if(node.getLeft() != null)
        {
            node.getLeft().apply(this);
        }

        if(node.getRight() != null)
        {
            node.getRight().apply(this);
        }
        outASemicolonExpr(node);
        output += "\n";
        spaces = spaces.substring(0, spaces.length()-4);
    }

    @Override
    public void caseAParallelExpr(AParallelExpr node)
    {
    	output += spaces + "||\n";
    	spaces += "    ";
        inAParallelExpr(node);
        if(node.getLeft() != null)
        {
            node.getLeft().apply(this);
        }

        output += "\n";

        if(node.getRight() != null)
        {
            node.getRight().apply(this);
        }
        outAParallelExpr(node);
        output += "\n";
        spaces = spaces.substring(0, spaces.length()-4);
    }

    @Override
    public void caseAParenthesisExpr(AParenthesisExpr node)
    {
        inAParenthesisExpr(node);
		output += spaces + "(\n";
		spaces += "    ";
        if(node.getExpr() != null)
        {
            node.getExpr().apply(this);
        }
        spaces = spaces.substring(0, spaces.length()-4);
        output += spaces + ")\n";
        outAParenthesisExpr(node);
    }

    @Override
	public void inAFunctionExpr(AFunctionExpr node)
	{
		output += spaces + node.getName().getText() + "(";

		for (TId id : node.getArgs()) {
			output += super.removeWhiteChar(id) + ",";
		}

		output = output.substring(0, output.length()-1);
		output += ")";
	}

	@Override
	public void inAPairExpr(APairExpr node)
	{
		output += spaces + "(" + removeWhiteChar(node.getFirst()) + ","
	                  + removeWhiteChar(node.getSecond()) + ")";
	}

	@Override
	public void inAZeroExpr(AZeroExpr node)
	{
		output += spaces + "0";
	}
}
