package edu.unibo.cs.STDLock;

public class PairOfStrings
{
	public String x;
	public String y;

	public PairOfStrings(String x, String y)
	{
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString()
	{
		return "("+x+","+y+")";
	}
}
