package edu.unibo.cs.STDLock;

import edu.unibo.cs.STDLock.LAM.analysis.DepthFirstAdapter;
import edu.unibo.cs.STDLock.LAM.node.AFunctionExpr;
import edu.unibo.cs.STDLock.LAM.node.APairExpr;
import edu.unibo.cs.STDLock.LAM.node.AParallelExpr;
import edu.unibo.cs.STDLock.LAM.node.AParenthesisExpr;
import edu.unibo.cs.STDLock.LAM.node.ASemicolonExpr;
import edu.unibo.cs.STDLock.LAM.node.AZeroExpr;
import edu.unibo.cs.STDLock.LAM.node.TId;

public class ASTDepthInlinePrinter extends DepthFirstAdapter
{
	protected String output;
	public ASTDepthInlinePrinter() {
		output = new String();
		output = "";
	}

	protected String removeWhiteChar(TId str)
	{
		assert(str.getText() != null);
		return str.getText().replaceAll("\\s", "");
	}

	@Override
	public void inAFunctionExpr(AFunctionExpr node)
	{
		output += node.getName().getText() + "(";

		for (TId id : node.getArgs()) {
			output += removeWhiteChar(id) + ",";
		}

		output = output.substring(0, output.length()-1);
		output += ")";
	}

	@Override
	public void inAPairExpr(APairExpr node)
	{
		output += "(" + removeWhiteChar(node.getFirst()) + ","
	                  + removeWhiteChar(node.getSecond()) + ")";
	}

	@Override
	public void inAZeroExpr(AZeroExpr node)
	{
		output += "0";
	}

	@Override
	public void caseAParallelExpr(AParallelExpr node)
	{
		inAParallelExpr(node);
        if(node.getLeft() != null)
        {
            node.getLeft().apply(this);
        }

        output += "||";

        if(node.getRight() != null)
        {
            node.getRight().apply(this);
        }
        outAParallelExpr(node);
	}

	@Override
	public void caseAParenthesisExpr(AParenthesisExpr node)
	{
		inAParenthesisExpr(node);
		output += "(";
        if(node.getExpr() != null)
        {
            node.getExpr().apply(this);
        }
        output += ")";
        outAParenthesisExpr(node);
	}

	@Override
	public void caseASemicolonExpr(ASemicolonExpr node)
    {
        inASemicolonExpr(node);
        if(node.getLeft() != null)
        {
            node.getLeft().apply(this);
        }

        output += ";";

        if(node.getRight() != null)
        {
            node.getRight().apply(this);
        }
        outASemicolonExpr(node);
    }

	public String getPrinted()
	{
		return output;
	}
}