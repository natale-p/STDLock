package edu.unibo.cs.STDLock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Logger;

/** Represent a mutation (see paper to know exactly what a mutation is)
 *
 * You could think a Mutation like a List: (x,x',x'',x'''), where each x is a
 * number between 0 and 2*(n-1), where n is the number of elements in the Mutation.
 *
 *
 * @author Natale Patriciello
 *
 */
public class Mutation {

	private final List<Integer> mutation;
	private Set<List<Integer>> boundSink;
	private Set<List<Integer>> freeSink;
	private Set<HashSet<Integer>> cycles;
	private int order;

	private static Logger logger=Logger.getLogger("edu.unibo.cs.STDLock.Mutation");

	/** Calculate the GCD between a and b
	 *
	 * @param a
	 * @param b
	 * @return GCD between a and b
	 */
	private static long gcd(long a, long b)
	{
	    while (b > 0)
	    {
	        long temp = b;
	        b = a % b;
	        a = temp;
	    }
	    return a;
	}

	private static long lcm(long a, long b)
	{
	    return a * (b / gcd(a, b));
	}

	/**
	 * Calculate the lower common multiple between numbers
	 *
	 * @param input input numbers
	 * @return the lcm
	 */
	public static long lcm(long[] input)
	{
		if (input.length <= 0) {
			return 0;
		}

	    long result = input[0];
	    for(int i = 1; i < input.length; i++) {
	    	result = lcm(result, input[i]);
	    }
	    	return result;
	}


	/** Create a Mutation from a List of integer
	 *
	 * @see calc
	 * @param arg0 List of integer from which build the mutation.
	 */
	public Mutation(List<Integer> arg0)
	{
		mutation = arg0;
		order = -1;
		calc();
	}

	@Override
	public String toString()
	{
		return mutation.toString();
	}


	/**
	 * Return the order of the mutation.
	 * The order of the mutation is max{a,b}, where
	 * a is the length of the maximum free sinks, and
	 * b is the sum between the length of maximum bound sink
	 * and the lower common multiple between length of cycles.
	 *
	 * @return the order of the mutation
	 */
	public int getOrder()
	{
		if (order > 0)
			return order;

		int max_free_sink = 0;
		for (List<Integer> sink : freeSink) {
			if (sink.size()-1 > max_free_sink) {
				max_free_sink = sink.size()-1;
			}
		}

		logger.fine("Max free sink length: " + max_free_sink);

		int max_bound_sink = 0;

		for (List<Integer> bound : boundSink) {
			if (bound.size()-1 > max_bound_sink) {
				max_bound_sink = bound.size()-1;
			}
		}

		logger.fine("Max bound sink length: " + max_bound_sink);

		long[] lengths = new long[cycles.size()];
		int i = 0;

		for(HashSet<Integer> cycle : cycles) {
			lengths[i] = cycle.size();
			i++;
		}

		long lcmOfCycles = lcm(lengths);

		logger.fine("lcm of cycles: " + lcmOfCycles);


		if (max_free_sink > max_bound_sink+lcmOfCycles) {
			order = max_free_sink;
		} else {
			order = (int) (max_bound_sink+lcmOfCycles);
		}

		return order;
	}

	/**
	 * Get children of a node.
	 *
	 * The children are all nodes with "i" in itself.
	 *
	 * @param i Node of which you want children
	 * @return List of children
	 */
	private List<Integer> getChildrenOf(Integer i)
	{
		List<Integer> childs = new ArrayList<Integer>();

		for(int h=0; h<mutation.size(); h++) {
			Integer mu = mutation.get(h);
			if (mu == i) {
				childs.add(h);
			}
		}

		return childs;
	}

	/**
	 * Get the father of a node.
	 *
	 * The father of the node is the number represented in the node
	 * itself.
	 *
	 * @param i Node of which you want father
	 * @return The father of the node
	 */
	private Integer getFatherOf(Integer i)
	{
		return mutation.get(i);
	}

	private void firstPart(Set<HashSet<Integer>> C, Set<Integer> M)
	{
		logger.finer("mutation=" + mutation.toString());

		//logger.fine("Inizializzo N, S, T, M, C e inizio la prima parte dell'algo");
		List<Integer> N = new ArrayList<Integer>();
		for (int i = 0; i < mutation.size(); i++) {
			N.add(i);
		}

		//logger.fine("N=" + N.toString());

		Stack<Integer> S = new Stack<Integer>();
		HashMap<Integer, List<Integer>> T = new HashMap<Integer, List<Integer>>();

		while (! N.isEmpty()) {
			Integer i = N.get(0);
			S.add(i);
			T.put(i, getChildrenOf(i));

			//logger.fine("Prendo un nuovo nodo da valutare e lo metto sopra allo stack: " + i);

			while (! S.isEmpty()) {
				Integer nodo = S.lastElement(); // == S.get(S.size()-1)
				List<Integer> figli = T.get(nodo);

				//logger.fine("Estraggo " + nodo + " dallo stack, e ha figli " + figli.toString());

				if (figli.size() <= 0) {
					//logger.fine("Non ha figli da visitare");
					if (getChildrenOf(nodo).size() <= 0) {
						//logger.fine("Non ha figli in generale! Sink");
						M.add(nodo);
					}

					N.remove(nodo);
					T.remove(nodo);
					S.remove(S.size()-1);
				} else {
					Integer figlio = figli.get(figli.size()-1);
					figli.remove(figli.size()-1);

					//logger.fine("Ho preso suo figlio " + figlio);

					if (S.contains(figlio)) {
						List<Integer> cyc = S.subList(S.indexOf(figlio), S.size());
						C.add(new HashSet<Integer>(cyc));
						//logger.fine("Wow, ciclo: " + cyc.toString());
					} else {
						//logger.fine("Non c'è sullo stack; lo aggiungo.");
						S.add(figlio);
						T.put(figlio, getChildrenOf(figlio));
					}
				}
			}
		}

		cycles = C;
	}

	private void secondPart(Set<HashSet<Integer>> C, Set<Integer> M)
	{
		Stack<Integer>     S = new Stack<Integer>();
		Set<List<Integer>> B = new HashSet<List<Integer>> ();
		Set<List<Integer>> F = new HashSet<List<Integer>> ();
		Iterator<Integer> MIt = M.iterator();

		while (MIt.hasNext()) {
			S.clear();
			S.add(MIt.next());

			while (!S.isEmpty()) {
				Integer nodo = S.lastElement();
				//logger.fine("Valuto "+ nodo);

				if (getFatherOf(nodo) < mutation.size()) {
					//logger.fine("Il suo padre " + mutation.get(nodo) + " è all'interno, cerco cicli");
					boolean found = false;

					Iterator<HashSet<Integer>> Cit = C.iterator();
					while(!found && Cit.hasNext()) {
						HashSet<Integer> cycle = Cit.next();

						if (cycle.contains(getFatherOf(nodo))) {
							@SuppressWarnings("unchecked")
							List<Integer> sink = (List<Integer>) S.clone();
							sink.add(getFatherOf(nodo));
							//logger.fine("Un ciclo ["+cycle.toString()+"] contiene il valore, "+ sink.toString() +" è un bound sink");
							B.add(sink);
							S.clear();
							found = true;
						}
					}

					if (! found) {
						//logger.fine("Nessun ciclo lo contiene, metto sullo stack");
						S.add(mutation.get(nodo));
					}
				} else {
					@SuppressWarnings("unchecked")
					List<Integer> sink = (List<Integer>) S.clone();
					sink.add(mutation.get(nodo));
					//logger.fine("il valore è fuori dalla mutation, "+ sink.toString() +" è un free sink");
					F.add(sink);
					S.clear();
				}
			}
		}

		//logger.fine("Free Sink "+ F.toString());
		//logger.fine("Bound sink " + B.toString());

		freeSink = F;
		boundSink = B;
	}

	/**
	 * Discover Bound Sink, Free Sink and cycles of the mutation,
	 * with an algorithm (by Natale Patriciello) described in his
	 * thesis.
	 */
	private void calc()
	{

		Set<Integer>          M = new HashSet<Integer>();
		Set<HashSet<Integer>> C = new HashSet<HashSet<Integer>>();

		firstPart(C, M);

		//logger.fine("Cycles: " + C.toString() + " MaybeSink: " + M.toString());

		secondPart(C, M);
	}
}
