package edu.unibo.cs.STDLock;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/** Context for parameters substitutions
 *
 * When a parameters instantiation is requested (for example when
 * calculating a mutation, or when check for deadlock in the
 * saturated history) the Context give the initial and successive
 * (through fresh names) instantiations, in Integer notation (so,
 * first is 0, second 1 and so on).
 *
 * If we have the program:
 * <pre>
 * {@code
 * M(x,y,z)=(x,z)||(z,y)||M(z,y,x)
 * }
 *</pre>
 * x, y and z are the formal arguments of M. If we need to instantiate them through
 * real values, you should ask to a Context class.
 * <pre>
 * {@code
 * Method m = ... ();
 * Context ctx = new Context();
 * List<Integer> args = ctx.getInitialArgs();
 *
 * //Do something with M and it's args
 * }
 * </pre>
 *
 * If you need a Context which works with string, please take a look into StringContext.
 * @author Natale Patriciello
 *
 */
public class IntegerContext {

	private int counter = 0;
	private List<Integer> initial = null;
	private List<Integer> fresh = null;
	private static Logger logger=Logger.getLogger("edu.unibo.cs.STDLock.Context");

	/** Make an empty context
	 *
	 */
	public IntegerContext() {
		fresh = new ArrayList<Integer>();
		initial = new ArrayList<Integer>();
	}

	/** Return initial names
	 *
	 * @param count Count of the values to be returned
	 * @return a List of names (which are represented by Integer)
	 */
	public List<Integer> getInitialValues(int count) {

		if (initial.size() > 0) {
			return null;
		}

		List<Integer> args = new ArrayList<Integer>();

		if (count < 0)
			return args;

		for(int i=0; i<count; i++) {
			args.add(i);
		}

		initial.addAll(args);

		counter += count;

		return args;
	}

	/** Return a list of fresh values
	 *
	 * @param count Count of fresh values requested
	 * @return List of fresh values
	 */
	public List<Integer> getFreshValues(int count) {
		List<Integer> args = new ArrayList<Integer>();

		if (count < 0)
			return args;

		for(int i=counter; i<counter+count; i++) {
			args.add(i);
		}

		logger.fine("Restituisco nomi fresh: " + args.toString());

		fresh.addAll(args);
		counter += count;

		return args;
	}

	/** Compare the arguments and return the Mutation associated
	 *
	 * It takes a list which represent the final permutation on the initial names,
	 * returned from a call of getInitialArgs (from the same object).
	 * Then, following the algorithm (see paper) it will return the Mutation
	 * associated.
	 *
	 * @param end list of arguments
	 * @return Mutation
	 */
	public Mutation compareValues(List<Integer> end) {
		if (end.size() != initial.size()) {
            return null;
		}

		ArrayList<Integer> myMut = new ArrayList<Integer>();

		for (int i=0; i<initial.size(); i++) {
			if (initial.contains(end.get(i))) {
				myMut.add(initial.indexOf(end.get(i)));
				logger.fine("In posizione " + i +" ho trovato, alla fine, il valore "
						+ end.get(i)+ " che è in posizione " + initial.indexOf(end.get(i)) +
						" all'inizio");
			} else {
				if (fresh.contains(end.get(i))) {
					myMut.add(initial.size()+ fresh.indexOf(end.get(i)));
					logger.fine("In posizione " + i + " ho trovato un fresh, che è poi in posizione "
							+ fresh.indexOf(end.get(i)) + " nella lista dei fresh, + " + initial.size());
				} else {
					logger.fine("STOOP! CROLLA TUTTO!" + fresh);
				}
			}
		}

		return new Mutation(myMut);
	}
}
