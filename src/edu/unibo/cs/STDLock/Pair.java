package edu.unibo.cs.STDLock;

public class Pair<E,F> {

	private final E p1;
	private final F p2;
	public Pair (E p1, F p2)
	{
		this.p1 = p1;
		this.p2 = p2;
	}

	public E getLeft()
	{
		return p1;
	}

	public F getRight()
	{
		return p2;
	}

	@Override
	public String toString()
	{
		return "<"+p1.toString()+">";
	}

}
