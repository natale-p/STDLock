package edu.unibo.cs.STDLock;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import edu.unibo.cs.STDLock.LAM.node.APairExpr;
import edu.unibo.cs.STDLock.LAM.node.AParallelExpr;
import edu.unibo.cs.STDLock.LAM.node.AParenthesisExpr;
import edu.unibo.cs.STDLock.LAM.node.ASemicolonExpr;
import edu.unibo.cs.STDLock.LAM.node.AZeroExpr;
import edu.unibo.cs.STDLock.LAM.node.PExpr;
import edu.unibo.cs.STDLock.LAM.node.Start;

public class CircularitiesFinder
{
	private boolean hasCirc = false;

	public static class Hen
	{
		public String head;
		public List<Hen> tail;
		public boolean visited = false;
		public boolean met = false;

		public Hen()
		{
			visited = false;
		}

		@Override
		public String toString()
		{
			String res = "("+this.head+"-->[";

			for (int i=0; i<tail.size()-1; i++) {
				res += tail.get(i).head + ",";
			}
			if (tail.size()>0)
				res += tail.get(tail.size()-1).head + "]";
			else
				res += "]";

			res += ")";

			return res;
		}
	}

	public List<PairOfStrings> whoCaused = null;

	public boolean hasCirc()
	{
		return hasCirc;
	}

	public CircularitiesFinder(Start abstractSyntaxTree)
	{
		this(abstractSyntaxTree.getPExpr());
	}

	public CircularitiesFinder(PExpr expr)
	{
		this(process(expr));
	}

	public CircularitiesFinder(List<List<PairOfStrings>> s)
	{
		//System.out.println(s);
		//assert(s.size() > 0);
		//long startC = System.nanoTime();
		//boolean hasCircC = checkCircCarlo(s);
		//long endC = System.nanoTime();

		//long start = System.nanoTime();
		hasCirc = checkCircMio(s);
		//long end = System.nanoTime();

		//assert(hasCirc == hasCircC);

		//System.out.println("IO: " + ((end-start)/1000000) + " C: " + ((endC-startC)/1000000));
	}

	public CircularitiesFinder()
	{

	}

	private static Hen contains(List<Hen> dC, String head)
	{
		for (Hen s : dC) {
			if (s.head.equals(head))
				return s;
		}

		Hen s = new Hen();
		s.head = head;
		s.tail = new ArrayList<Hen>();

		dC.add(s);
		return s;
	}

	public static List<Hen> buildHenFrom(List<PairOfStrings> l)
	{
		List<Hen> dependencyChain = new ArrayList<Hen>();
		for (PairOfStrings s : l) {
			Hen hen_x = contains(dependencyChain, s.x);
			Hen hen_y = contains(dependencyChain, s.y);
			hen_x.tail.add(hen_y);
		}

		return dependencyChain;
	}

	public boolean checkCircCarlo(List<List<PairOfStrings>> states)
	{
		assert(states.size() > 0);

		for (List<PairOfStrings> single_state : states) {
			List<Hen> h = buildHenFrom(single_state);

			boolean deadLockExist = checkCarlo(h);

			if (deadLockExist) {
				whoCaused = single_state;
				return true;
			}
		}

		return false;
	}

	public boolean checkCircMio(List<List<PairOfStrings>> states)
	{
		assert(states.size() > 0);

		for (List<PairOfStrings> single_state : states) {
			List<Hen> h = buildHenFrom(single_state);

			boolean deadLockExist = checkMio(h);

			if (deadLockExist) {
				whoCaused = single_state;
				return true;
			}
		}

		return false;
	}


	private static boolean recursiveFind(String head, Hen h, List<Hen> hens)
	{
		//System.out.println("Cerco " + head + " in " + h);

		for (Hen t : h.tail) {
			//System.out.println("Controllo il caso banale, h=" + head + " t="+t);
			if (t.head.equals(head))
				return true;
		}

		for (Hen ofT : h.tail) {

			if (ofT.visited) {
				//System.out.println("Già passato in " + t );
				continue;
			}

			ofT.visited = true;

			if (recursiveFind(head, ofT, hens))
				return true;
		}

		return false;
	}

	public static boolean checkCarlo(List<Hen> hens)
	{
		boolean exit = false;
		for (Hen h : hens) {
			if (recursiveFind(h.head, h, hens))
				exit = true;

			for (Hen res : hens)
				res.visited = false;

			if (exit)
				return true;
		}

		return false;
	}

	private static boolean recursiveFindWithStory(Hen v, Stack<Hen> S)
	{
		v.visited = true;
		S.push(v);

		for (Hen w : v.tail) {
			if (! w.visited) {
				if (recursiveFindWithStory(w, S))
					return true;
			} else if (S.contains(w))
				return true;
		}

		S.pop();

		return false;
	}

	public static boolean checkMio(List<Hen> hens)
	{
		Stack<Hen> S = new Stack<Hen>();
		for (Hen h : hens) {
			if (!h.visited && recursiveFindWithStory(h, S))
				return true;
		}

		return false;
	}

	private static List<List<PairOfStrings>> cons(List<List<PairOfStrings>> first, List<List<PairOfStrings>> second)
	{
		List<List<PairOfStrings>> ret = new ArrayList<List<PairOfStrings>>();
		ret.addAll(first);
		ret.addAll(second);

		return ret;
	}

	private static List<List<PairOfStrings>> cartesian(List<List<PairOfStrings>> A, List<List<PairOfStrings>> B)
	{
		List<List<PairOfStrings>> ret = new ArrayList<List<PairOfStrings>>();

		if (A.isEmpty()) {
			ret.addAll(B);
			return ret;
		}

		if (B.isEmpty()) {
			ret.addAll(A);
			return ret;
		}


		for (List<PairOfStrings> inA : A) {
			for (List<PairOfStrings> inB : B) {
				List<PairOfStrings> n = new ArrayList<PairOfStrings>();

				n.addAll(inA);
				n.addAll(inB);

				ret.add(n);
			}
		}

		return ret;
	}

	public static List<List<PairOfStrings>> process(PExpr node)
	{
		List<List<PairOfStrings>> ret = new ArrayList<List<PairOfStrings>>();

		if (node instanceof AParallelExpr) {

			PExpr left = ((AParallelExpr) node).getLeft();
			PExpr right = ((AParallelExpr) node).getRight();

			List<List<PairOfStrings>> l = process(left);
			List<List<PairOfStrings>> r = process(right);

			return cartesian(l,r);
		} else if (node instanceof ASemicolonExpr) {
			List<List<PairOfStrings>> right = process(((ASemicolonExpr) node).getRight());
			List<List<PairOfStrings>> left = process(((ASemicolonExpr) node).getLeft());

			return cons(left, right);
		} else if (node instanceof AParenthesisExpr) {
			return process(((AParenthesisExpr) node).getExpr());
		} else if (node instanceof APairExpr) {
			String x = ((APairExpr) node).getFirst().toString().replaceAll("\\s", "");
			String y = ((APairExpr) node).getSecond().toString().replaceAll("\\s", "");
			List<PairOfStrings> base = new ArrayList<PairOfStrings>();
			base.add(new PairOfStrings(x,y));
			ret.add(base);
		} else {
			assert(node instanceof AZeroExpr);
		}

		return ret;
	}
}
