package edu.unibo.cs.STDLock;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import edu.unibo.cs.STDLock.LAM.node.AFunctionExpr;

public class SaturateMethod {

	private static Logger logger=Logger.getLogger("edu.unibo.cs.STDLock.SaturatedMethodTree");

	private final Map<Method, Method> methodToSaturatedTree;

	public SaturateMethod()
	{
		methodToSaturatedTree = new HashMap<Method, Method>();
	}

	private Partition<Method> getPartitionOf(Method M, Set<Partition<Method>> partitions)
	{
		for (Partition<Method> part : partitions) {
			if (part.contains(M))
				return part;
		}

		return null;
	}

	// TODO: Add a cache
	public Method saturate(Method m, Set<Partition<Method>> partitions, Set<Method> allMethods)
	{
		if (methodToSaturatedTree.containsKey(m))
			return new Method(methodToSaturatedTree.get(m));

		logger.fine("Creating saturated tree for " + m);
		Method saturated = new Method(m);

		if (m.children.size() == 0)
			return saturated;

		Partition<Method> partitionOfM = getPartitionOf(m, partitions);

		if (partitionOfM != null) {
			Method PEm = partitionOfM.getEP(saturated);
			Method PEr = partitionOfM.getEP(partitionOfM.get(0));
			StringContext ctx = new StringContext();
			PEm.initStringContext(ctx);

			logger.fine("EP from " + m + " => " + PEm.ASTToString());
			logger.fine("EPr from " + partitionOfM.get(0) + " => " + PEr.ASTToString());

			logger.fine("Merging " + partitionOfM.getOrder()*2 + " times EPr..");
			for (int i=0; i<partitionOfM.getOrder()*2; i++) {
				Method PErToMerge = new Method(PEr);
				Pair<AFunctionExpr, Method> invkOfPEr = PEm.getFirstInvocationOf(PEr);

				assert(invkOfPEr != null);

				PEm.merge(invkOfPEr.getLeft(), PErToMerge, ctx);
				PEm.mergeChildrenOf(invkOfPEr, PErToMerge);
				PEm.removeChild(invkOfPEr);
			}

			PEm.removeCallTo(PEm.getFirstInvocationOf(PEr));
			saturated = PEm;
		}


		StringContext ctx = new StringContext();
		saturated.initStringContext(ctx);

		Iterator<Pair<AFunctionExpr, Method>> it = saturated.children.iterator();

		while (it.hasNext()) {
			Pair<AFunctionExpr, Method> p = it.next();

			Method saturatedChild = saturate(p.getRight(), partitions, allMethods);
			saturated.merge(p.getLeft(), saturatedChild, ctx);

			it.remove();
		}

		assert(saturated.getChildren().size()==0);

		/*CircularitiesFinder cf = new CircularitiesFinder(saturated.getAST());

		if (cf.hasCirc()) {
			System.out.println("I already know that + " + saturated.getNameMerge() +
					" has a deadlock caused by " + cf.whoCaused);
		}*/

		methodToSaturatedTree.put(m, new Method(saturated));
		return saturated;
	}
}
