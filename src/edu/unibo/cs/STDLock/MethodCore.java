package edu.unibo.cs.STDLock;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PushbackReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import edu.unibo.cs.STDLock.LAM.analysis.DepthFirstAdapter;
import edu.unibo.cs.STDLock.LAM.lexer.Lexer;
import edu.unibo.cs.STDLock.LAM.lexer.LexerException;
import edu.unibo.cs.STDLock.LAM.node.AFunctionExpr;
import edu.unibo.cs.STDLock.LAM.node.APairExpr;
import edu.unibo.cs.STDLock.LAM.node.Start;
import edu.unibo.cs.STDLock.LAM.node.TId;
import edu.unibo.cs.STDLock.LAM.parser.Parser;
import edu.unibo.cs.STDLock.LAM.parser.ParserException;

public abstract class MethodCore {

	protected String name;
	protected String nameMerge;
	protected  int argsCount;

	protected String definition;

	protected List<Pair<AFunctionExpr, Method>> children;
	protected List<List<Method>> history;
	protected Set<TId> boundVariables;
	protected Set<TId> freeVariables;

	protected Partition<Method> myPartition;

	protected Start ast;

	/** Create a method
	 *
	 * @param name Name of the method
	 * @param definition Definition of the method in the form signature=body
	 * @param argsCount Number of arguments taken in input by the method itself
	 * @throws Exception
	 */
	public MethodCore(String name, String definition) throws Exception {
		children = new LinkedList<Pair<AFunctionExpr, Method>>();
		myPartition = null;
		nameMerge = new String(name);
		this.history = new ArrayList<List<Method>>();
		this.name = name;

		if (! name.matches("[A-Za-z]+[0-9]*")) {
			throw new Exception("Name " + name +" is not valid. Valid names are in the form [A-Za-z]+[0-9]*");
		}

		this.definition = definition;

		String sign = getSignature();
		if (sign == null) {
			this.definition = new String();
			this.argsCount = 0;
			return;
		}

		if (sign.contains(",")) {
			this.argsCount = sign.split(",").length;
		} else {
			if (sign.matches("[a-zA-Z]+[0-9]*"+"\\(\\)")) {
				this.argsCount = 0;
			} else {
				this.argsCount = 1;
			}
		}

		initAST();
	}


	/** Create a new method from another
	 *
	 * @param other Other method from which copy everything, to make
	 * this and other equals (but not the same)
	 */
	public MethodCore(Method other) {
		history = other.getHistory();
		myPartition = other.getPartition();
		this.name = other.getName();
		nameMerge = other.getNameMerge();
		this.argsCount = other.getArgsCount();
		this.definition = other.getDefinition();
		this.ast = (Start) other.getAST().clone();

		initVar();

		ASTDepthWalkerForChildren walker = new ASTDepthWalkerForChildren(other.getChildren());
		ast.apply(walker);

		children = walker.nodeToMethod;
	}

	protected class ASTDepthWalkerForVariables extends DepthFirstAdapter {

		public final List<TId> variables;

		public ASTDepthWalkerForVariables()
		{
			super();
			variables = new ArrayList<TId>();
		}

		@Override
		public void inAPairExpr(APairExpr node)
		{
			variables.add(node.getFirst());
			variables.add(node.getSecond());
		}

		@Override
		public void inAFunctionExpr(AFunctionExpr node)
		{
			for (TId var : node.getArgs()) {
				variables.add(var);
			}
		}
	}

	protected class ASTDepthWalkerForChildren extends ASTDepthWalkerForVariables {

		private final Collection<Method> allMethods;
		public final List<Pair<AFunctionExpr, Method>> nodeToMethod;

		public ASTDepthWalkerForChildren(Collection<Method> allMethods)
		{
			super();
			this.allMethods = allMethods;
			nodeToMethod = new LinkedList<Pair<AFunctionExpr, Method>>();
		}

		public ASTDepthWalkerForChildren(List<Pair<AFunctionExpr, Method>> children)
		{
			super();
			this.allMethods = new ArrayList<Method>();

			for (Pair<AFunctionExpr, Method> x : children) {
				allMethods.add(x.getRight());
			}

			nodeToMethod = new LinkedList<Pair<AFunctionExpr, Method>>();
		}

		@Override
		public void inAFunctionExpr(AFunctionExpr node)
		{
			super.inAFunctionExpr(node);
			String nodeName = node.getName().getText().replaceAll("\\s","");

			for (Method m : allMethods) {
				if (m.getName().equals(nodeName)) {
					nodeToMethod.add(new Pair<AFunctionExpr, Method>(node, m));
					break;
				}
			}
		}
	}

	/**
	 * Rebuild the children list, finding all methods called by this.
	 *
	 * @param allMethods Set of all methods
	 */
	public void rebuildChildrenList(Set<Method> allMethods)
	{
		ASTDepthWalkerForChildren walker = new ASTDepthWalkerForChildren(allMethods);
		ast.apply(walker);

		children = walker.nodeToMethod;

		initVar();

		// BIG: Se non uso gli argomenti formali nel body, senza
		// questo snippet sono "dimenticati"

		/*
		String formals[] = getSignature().split("\\)")[0].split("\\(")[1].split(",");

		if (boundVariables.size() != formals.length) {
			for (String formal : formals) {
				boolean found = false;

				for (TId bound : boundVariables) {
					if (bound.getText().equals(formal)) {
						found = true;
						break;
					}
				}

				if (found == false)
					boundVariables.add(new TId(formal));
			}
		}*/
	}

	protected void initAST() throws ParserException, LexerException, IOException
	{
		boundVariables = new HashSet<TId>();
		freeVariables = new HashSet<TId>();

		/* Con questo controllo che la signature sia
		 * una valida chiamata a funzione
		 */
		Lexer lexer = new Lexer (new PushbackReader(new BufferedReader(new StringReader(getSignature()), 1024)));
		Parser parser = new Parser(lexer);
		ast = parser.parse();

		lexer = new Lexer (new PushbackReader(new BufferedReader(new StringReader(getBody()), 1024)));
		parser = new Parser(lexer);

		ast = parser.parse();

		initVar();
	}

	protected void initVar()
	{
		boundVariables = new HashSet<TId>();
		freeVariables = new HashSet<TId>();

		ASTDepthWalkerForVariables walker = new ASTDepthWalkerForVariables();
		ast.apply(walker);

		String[] param = getSignature().split("\\)")[0].split("\\(")[1].split(",");

		List<String> parameters = new ArrayList<String>();
		for (int i=0; i<param.length; i++)
			parameters.add(param[i]);

		List<TId> variables = walker.variables;

		for (TId var : variables) {
			String id = var.toString().replaceAll("\\s","");

			if (parameters.contains(id)) {
				boundVariables.add(var);
			} else {
				freeVariables.add(var);
			}
		}
	}

	public Partition<Method> getPartition()
	{
		return myPartition;
	}

	public void setPartition(Partition<Method> part)
	{
		myPartition = part;
	}

	public Set<TId> getBound()
	{
		return new HashSet<TId>(boundVariables);
	}

	public Set<TId> getFree()
	{
		return  new HashSet<TId>(freeVariables);
	}

	public void initStringContext(StringContext ctx)
	{
		String formals[] = getSignature().split("\\)")[0].split("\\(")[1].split(",");
		ctx.pushNames(formals);

		Set<String> var = new HashSet<String>();
		for (TId freevar : freeVariables) {
			var.add(freevar.toString().replaceAll("\\s",""));
		}

		ctx.pushNames(var);
	}

	public Start getAST()
	{
		return ast;
	}

	/** Get the arguments count
	 *
	 * @return the number of arguments that this method coould take in input
	 */
	public int getArgsCount() {
		return argsCount;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other)
			return true;

		if (! (other instanceof Method))
			return false;

		Method that = (Method) other;

		return (this.name.equals(that.getName()));
	}

	@Override
	public int hashCode()
	{
		return name.hashCode();
	}

	/** Get the method name
	 *
	 * @return The method name
	 */
	public String getName() {
		return new String(this.name);
	}

	/** Get the method definition
	 *
	 * @return Method definition
	 */
	public String getDefinition() {
		return new String(this.definition);
	}

	/** Add a child to this method
	 *
	 * This (conveniently) add this as a father of the other.
	 *
	 * @param method Child of this
	 * @return
	 */
	public boolean addChild(Method method) {
		return children.add(new Pair<AFunctionExpr, Method>(null, method));
	}

	/** Get children of this method
	 *
	 * You will own the list
	 *
	 * @return a list of children of this method
	 */
	public List<Pair<AFunctionExpr, Method>> getChildren() {
		List<Pair<AFunctionExpr, Method>> ret = new LinkedList<Pair<AFunctionExpr, Method>>();
		ret.addAll(children);

		return ret;
	}


	@Override
	public String toString() {
		return new String(name);
	}

	/**
	 * Add an history to this.
	 *
	 * Methods can have multiple histories.
	 *
	 * @param history History of the method
	 */
	public void addHistory(List<Method> history) {
		if (this.history.contains(history))
			return;

		this.history.add(history);
	}

	public void resetHistory()
	{
		history = new ArrayList<List<Method>>();
	}

	/**
	 * Get the history (you not own the List)
	 *
	 * @return Histories (if more than one) of the method
	 */
	public List<List<Method>> getHistory() {
		return history;
	}

	/** Get the signature of the method
	 *
	 * @return Method signature
	 */
	public String getSignature()
	{
		if (definition == null)
			return null;

		return definition.split("=")[0];
	}

	/**
	 * Get the body of the method
	 * @return Method's body
	 */
	public String getBody()
	{
		if (definition == null)
			return null;

		return definition.split("=")[1];
	}

	public String ASTToString()
	{
		ASTDepthInlinePrinter printer = new ASTDepthInlinePrinter();
		getAST().apply(printer);

		return printer.getPrinted();
	}

	public String getNameMerge()
	{
		return nameMerge;
	}

	public void setNameMerge(String name)
	{
		nameMerge = name;
	}

	public Pair<AFunctionExpr, Method> getFirstInvocationOf(Method N)
	{
		for (Pair<AFunctionExpr, Method> p : children) {
			if (p.getRight().equals(N))
				return p;
		}
		return null;
	}
}
