package edu.unibo.cs.STDLock.test;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.junit.Test;

import edu.unibo.cs.STDLock.ASTDepthInlinePrinter;
import edu.unibo.cs.STDLock.Method;
import edu.unibo.cs.STDLock.Partition;

public class PartitionTest {

	@Test
	public void testGetOrder() throws Exception {
		Method m = new Method("M", "M(x,y,z,u)=(x,z)||Q(u,y,z)");
		Method q = new Method("Q", "Q(x,y,z)=(x,y)||M(y,z,x,u)");
		Method p = new Method("P", "P(x,y,z,u)=(z,x)||P(x,y,z,u)||M(x,y,z,u)");

		Set<Method> allMethods = new HashSet<Method>();
		allMethods.add(m);
		allMethods.add(q);
		allMethods.add(p);

		m.rebuildChildrenList(allMethods);
		q.rebuildChildrenList(allMethods);
		p.rebuildChildrenList(allMethods);

		List<Method> aa = new ArrayList<Method>();
		aa.add(m);
		aa.add(q);

		Partition<Method> part = new Partition<Method>(aa);
		assertEquals(part.getOrder(), 4);

		aa.clear();
		aa.add(p);

		part = new Partition<Method>(aa);
		assertEquals(part.getOrder(), 1);
	}

	@Test
	public void testGenMTT() throws Exception
	{
		Logger logger = Logger.getLogger("");
	    logger.setLevel(Level.FINEST);
	    try {
			FileHandler fileTxt = new FileHandler("Logging.txt");
			fileTxt.setFormatter(new SimpleFormatter());
		    logger.addHandler(fileTxt);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Method m = new Method("M", "M(x,y)=(x,y)||N(x);(y,z)||H(z,y);H(x,y)");
		Method n = new Method("N", "N(x)=(x,y);O(y)");
		Method o = new Method("O", "O(x)=(x,y)||P(y)");
		Method p = new Method("P", "P(x)=M(x,y)");
		Method h = new Method("H", "H(x,y)=(y,x)");

		Set<Method> allMethods = new HashSet<Method>();
		allMethods.add(m);
		allMethods.add(n);
		allMethods.add(o);
		allMethods.add(p);

		m.rebuildChildrenList(allMethods);
		n.rebuildChildrenList(allMethods);
		o.rebuildChildrenList(allMethods);
		p.rebuildChildrenList(allMethods);

		ArrayList<Method> aa = new ArrayList<Method>();
		aa.add(m);
		aa.add(n);
		aa.add(o);
		aa.add(p);

		Partition<Method> part = new Partition<Method>(aa);
		Set<Partition<Method>> partitions = new HashSet<Partition<Method>>();
		partitions.add(part);


		allMethods.add(h);
		h.rebuildChildrenList(allMethods);

		ASTDepthInlinePrinter pr = new ASTDepthInlinePrinter();
		part.getEP(o).getAST().apply(pr);
		System.out.println(pr.getPrinted());
	}

	@Test
	public void testGetEP() throws Exception
	{
		Method m = new Method("M", "M(x,y)=(x,y)||N(x);(y,z)||H(z,y);H(x,y)");
		Method n = new Method("N", "N(x)=(x,y);O(y)");
		Method o = new Method("O", "O(x)=(x,y)||P(y)");
		Method p = new Method("P", "P(x)=M(x,y)");

		Set<Method> allMethods = new HashSet<Method>();
		allMethods.add(m);
		allMethods.add(n);
		allMethods.add(o);
		allMethods.add(p);

		m.rebuildChildrenList(allMethods);
		n.rebuildChildrenList(allMethods);
		o.rebuildChildrenList(allMethods);
		p.rebuildChildrenList(allMethods);

		ArrayList<Method> aa = new ArrayList<Method>();
		aa.add(m);
		aa.add(n);
		aa.add(o);
		aa.add(p);

		Partition<Method> part = new Partition<Method>(aa);

		System.out.println(part.getEP(n).ASTToString());
	}

}
