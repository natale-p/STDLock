package edu.unibo.cs.STDLock.test;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.junit.Test;

import edu.unibo.cs.STDLock.CircularitiesFinder;
import edu.unibo.cs.STDLock.Method;
import edu.unibo.cs.STDLock.MethodTree;
import edu.unibo.cs.STDLock.Partition;
import edu.unibo.cs.STDLock.SaturateMethod;

public class SaturateMethodTest
{
	@Test
	public void testStartSaturating() throws Exception {
		Logger logger = Logger.getLogger("");
    logger.setLevel(Level.FINEST);
    try {
		FileHandler fileTxt = new FileHandler("Logging.txt");
		fileTxt.setFormatter(new SimpleFormatter());
	    logger.addHandler(fileTxt);
	} catch (SecurityException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		Method m = new Method("M", "M(x)=(x,y);N(x);(H(x,y);H(y,x))");
		Method n = new Method("N", "N(x)=O(x)");
		Method o = new Method("O", "O(x)=P(x)");
		Method p = new Method("P", "P(x)=M(x)||F(y,z,w)");

		Method h = new Method("H", "H(x,y)=(x,y);I(x)");
		Method i = new Method("I", "I(x)=G(x)");
		Method g = new Method("G", "G(x)=H(x,y);F(y,z,w)");


		Method f = new Method("F", "F(x,y,z)=(x,y);E(z,y,x,w)");
		Method e = new Method("E", "E(x,y,z,w)=(x,w);F(w,z,x)");

		Set<Method> allMethods = new HashSet<Method>();
		allMethods.add(m);
		allMethods.add(n);
		allMethods.add(o);
		allMethods.add(p);

		allMethods.add(h);
		allMethods.add(i);
		allMethods.add(g);

		allMethods.add(f);
		allMethods.add(e);

		m.rebuildChildrenList(allMethods);
		n.rebuildChildrenList(allMethods);
		o.rebuildChildrenList(allMethods);
		p.rebuildChildrenList(allMethods);

		h.rebuildChildrenList(allMethods);
		i.rebuildChildrenList(allMethods);
		g.rebuildChildrenList(allMethods);

		f.rebuildChildrenList(allMethods);
		e.rebuildChildrenList(allMethods);

		MethodTree MT = new MethodTree(m);
		System.out.println("##==> Faccio partizioni");
		Set<Partition<Method>> partitions = MT.getPartitions();

		System.out.println("##==> Saturo ");
		SaturateMethod saturate = new SaturateMethod();
		Method mSat = saturate.saturate(m, partitions, allMethods);

		System.out.println("##==> Cerco deadlock alla fine in " + mSat.getNameMerge());

		CircularitiesFinder cf = new CircularitiesFinder(mSat.getAST());

		if (cf.hasCirc()) {
			System.out.println("### C'è dead in " + cf.whoCaused);
		} else {
			System.out.println("### Non c'è dead");
		}
	}

	@Test
	public void testStartSaturatingTesi() throws Exception {
		Logger logger = Logger.getLogger("");
    logger.setLevel(Level.FINEST);
    try {
		FileHandler fileTxt = new FileHandler("Logging.txt");
		fileTxt.setFormatter(new SimpleFormatter());
	    logger.addHandler(fileTxt);
	} catch (SecurityException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    System.out.println ("TESI!!");
		Method m = new Method("M", "M(x,y)=(x,y);N(x);(y,z)||H(z,y);H(y,x)");
		Method n = new Method("N", "N(x)=(x,y);O(x)");
		Method o = new Method("O", "O(x)=(x,y)||P(x)");
		Method p = new Method("P", "P(x)=M(x,y)||F(y,z,w)");

		Method h = new Method("H", "H(x,y)=I(x);(x,y)");
		Method i = new Method("I", "I(x)=G(x)||(x,y)");
		Method g = new Method("G", "G(x)=H(x,y);F(y,z,w)");


		Method f = new Method("F", "F(x,y,z)=(x,y);E(z,y,x,w)");
		Method e = new Method("E", "E(x,y,z,w)=(x,w);F(w,z,x)");

		Set<Method> allMethods = new HashSet<Method>();
		allMethods.add(m);
		allMethods.add(n);
		allMethods.add(o);
		allMethods.add(p);

		allMethods.add(h);
		allMethods.add(i);
		allMethods.add(g);

		allMethods.add(f);
		allMethods.add(e);

		m.rebuildChildrenList(allMethods);
		n.rebuildChildrenList(allMethods);
		o.rebuildChildrenList(allMethods);
		p.rebuildChildrenList(allMethods);

		h.rebuildChildrenList(allMethods);
		i.rebuildChildrenList(allMethods);
		g.rebuildChildrenList(allMethods);

		f.rebuildChildrenList(allMethods);
		e.rebuildChildrenList(allMethods);

		MethodTree MT = new MethodTree(m);
		System.out.println("##==> Faccio partizioni");
		Set<Partition<Method>> partitions = MT.getPartitions();

		System.out.println("##==> Saturo ");
		SaturateMethod saturate = new SaturateMethod();
		Method mSat = saturate.saturate(m, partitions, allMethods);

		System.out.println("##==> Cerco deadlock alla fine in " + mSat.getNameMerge());

		CircularitiesFinder cf = new CircularitiesFinder(mSat.getAST());

		if (cf.hasCirc()) {
			System.out.println("### C'è dead in " + cf.whoCaused);
		} else {
			System.out.println("### Non c'è dead");
		}
	}

	@Test
	public void testStartSaturatingParallelo() throws Exception {
		Logger logger = Logger.getLogger("");
    logger.setLevel(Level.FINEST);
    try {
		FileHandler fileTxt = new FileHandler("Logging.txt");
		fileTxt.setFormatter(new SimpleFormatter());
	    logger.addHandler(fileTxt);
	} catch (SecurityException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    System.out.println ("TUTTO PARALLELO!!");
		Method m = new Method("M", "M(x,y)=(x,y)||N(x)||(y,z)||H(w,z)||H(z,w)");
		Method n = new Method("N", "N(x)=(x,y)||O(x)");
		Method o = new Method("O", "O(x)=(x,y)||P(x)");
		Method p = new Method("P", "P(x)=M(x,y)||F(y,z,w)");

		Method h = new Method("H", "H(x,y)=I(x)||(x,y)");
		Method i = new Method("I", "I(x)=G(x)||(x,y)");
		Method g = new Method("G", "G(x)=H(x,y)||F(y,z,w)");


		Method f = new Method("F", "F(x,y,z)=(x,y)||E(z,y,x,w)");
		Method e = new Method("E", "E(x,y,z,w)=(x,w)||F(w,z,x)");

		Set<Method> allMethods = new HashSet<Method>();
		allMethods.add(m);
		allMethods.add(n);
		allMethods.add(o);
		allMethods.add(p);

		allMethods.add(h);
		allMethods.add(i);
		allMethods.add(g);

		allMethods.add(f);
		allMethods.add(e);

		m.rebuildChildrenList(allMethods);
		n.rebuildChildrenList(allMethods);
		o.rebuildChildrenList(allMethods);
		p.rebuildChildrenList(allMethods);

		h.rebuildChildrenList(allMethods);
		i.rebuildChildrenList(allMethods);
		g.rebuildChildrenList(allMethods);

		f.rebuildChildrenList(allMethods);
		e.rebuildChildrenList(allMethods);

		MethodTree MT = new MethodTree(m);
		System.out.println("##==> Faccio partizioni");
		Set<Partition<Method>> partitions = MT.getPartitions();

		System.out.println("##==> Saturo ");
		SaturateMethod saturate = new SaturateMethod();
		Method mSat = saturate.saturate(m, partitions, allMethods);

		System.out.println("##==> Cerco deadlock alla fine in " + mSat.getNameMerge());

		CircularitiesFinder cf = new CircularitiesFinder(mSat.getAST());

		if (cf.hasCirc()) {
			System.out.println("### C'è dead in " + cf.whoCaused);
		} else {
			System.out.println("### Non c'è dead");
		}
	}

	private void nonDeadlock(Method m, Set<Method> allMethods)
	{
		MethodTree MT = new MethodTree(m);
		Set<Partition<Method>> partitions = MT.getPartitions();
		SaturateMethod worker = new SaturateMethod();
		m = worker.saturate(m, partitions, allMethods);

		CircularitiesFinder cf = new CircularitiesFinder(m.getAST());
		assertTrue(!cf.hasCirc());
	}

	private void yeahDeadlock(Method m, Set<Method> allMethods)
	{
		MethodTree MT = new MethodTree(m);
		Set<Partition<Method>> partitions = MT.getPartitions();
		SaturateMethod worker = new SaturateMethod();
		m = worker.saturate(m, partitions, allMethods);

		CircularitiesFinder cf = new CircularitiesFinder(m.getAST());
		assertTrue(cf.hasCirc());
	}

	@Test
	public void testRegression() throws Exception
	{
		Method m = new Method("M", "M(x)=(x,y);(y,x)");
		Method m1 = new Method("M1", "M1(x)=((x,y);(x,y));((x,y);(y,x))");
		Method n = new Method("N", "N(x)=(x,y)||(y,x)");
		Method n0 = new Method("N0", "N0(x)=(x,y)||(x,y)||(y,z)||(z,y)");
		Method n1 = new Method("N1", "N1(x)=((a,b)||((c,d);(e,f)))||(g,h)");
		Method o = new Method("O", "O(x,y)=((x,y);(y,x));(y,x)");
		Method p = new Method("P", "P(x)=((x,y);(y,x))||(y,x)");
		Method q = new Method("Q", "Q(x)=((x,y);(y,x))||((y,x)||(x,y))");
		Method q1 = new Method("Q1", "Q1(x,y)=(x,y)||Q1(x,x)");

		Set<Method> allMethods = new HashSet<Method>();
		allMethods.add(m);
		allMethods.add(m1);
		allMethods.add(n);
		allMethods.add(n0);
		allMethods.add(n1);
		allMethods.add(o);
		allMethods.add(p);
		allMethods.add(q);
		allMethods.add(q1);

		m.rebuildChildrenList(allMethods);
		m1.rebuildChildrenList(allMethods);
		n.rebuildChildrenList(allMethods);
		n0.rebuildChildrenList(allMethods);
		n1.rebuildChildrenList(allMethods);
		o.rebuildChildrenList(allMethods);
		p.rebuildChildrenList(allMethods);
		q.rebuildChildrenList(allMethods);
		q1.rebuildChildrenList(allMethods);

		nonDeadlock(m, allMethods);
		nonDeadlock(m1, allMethods);
		yeahDeadlock(n, allMethods);
		yeahDeadlock(n0, allMethods);
		nonDeadlock(n1, allMethods);
		nonDeadlock(o, allMethods);
		yeahDeadlock(p, allMethods);
		yeahDeadlock(q, allMethods);
		yeahDeadlock(q1, allMethods);
	}

	@Test
	public void testpresentazione() throws Exception
	{
		System.out.println("PRES!");
		Method h = new Method("H", "H(x,y)=(x,y)||I(x,y)");
		Method i = new Method("I", "I(x,y)=(x,y)||H(u,v);(u,v)||F(u)");
		Method f = new Method("F", "F(x)=(x,y)||F(y)");
		Method g = new Method("G", "G(x,y)=(x,y);(x,y)||G(y,z)");


		Set<Method> allMethods = new HashSet<Method>();
		allMethods.add(h);
		allMethods.add(i);
		allMethods.add(f);
		allMethods.add(g);

		h.rebuildChildrenList(allMethods);
		i.rebuildChildrenList(allMethods);
		f.rebuildChildrenList(allMethods);
		g.rebuildChildrenList(allMethods);

		MethodTree MT = new MethodTree(g);
		Set<Partition<Method>> partitions = MT.getPartitions();
		SaturateMethod worker = new SaturateMethod();
		g = worker.saturate(g, partitions, allMethods);

		System.out.println("ASD" + g.ASTToString());
		CircularitiesFinder cf = new CircularitiesFinder(g.getAST());
	}
}
