package edu.unibo.cs.STDLock.test;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.junit.Test;

import edu.unibo.cs.STDLock.CircularitiesFinder;
import edu.unibo.cs.STDLock.Method;
import edu.unibo.cs.STDLock.MethodTree;
import edu.unibo.cs.STDLock.Partition;
import edu.unibo.cs.STDLock.SaturateMethod;

public class MethodTreeTest {

	/*
	@Test
	public void testMethodTree() throws Exception {
		Logger logger = Logger.getLogger("");
	    logger.setLevel(Level.FINEST);
	    try {
			FileHandler fileTxt = new FileHandler("Logging.txt");
			fileTxt.setFormatter(new SimpleFormatter());
		    logger.addHandler(fileTxt);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Method m = new Method("M", "M(x)=0");
		MethodTree s = new MethodTree(m);

		assertEquals(s.getRoot(), m);
	}

	private void littlePartition() throws Exception
	{
		Method m = new Method("M", "M(x)=N(x)");
		Method n = new Method("N", "N(x)=M(x)");

		Set<Method> allMethods = new HashSet<Method>();
		allMethods.add(m);
		allMethods.add(n);

		m.rebuildChildrenList(allMethods);
		n.rebuildChildrenList(allMethods);

		MethodTree MT = new MethodTree(m);
		Set<Partition<Method>> partitions = MT.getPartitions();

		assertTrue(partitions != null);
	}

	@Test
	public void testGetPartitions() throws Exception {
		Logger logger = Logger.getLogger("");
	    logger.setLevel(Level.FINEST);
	    try {
			FileHandler fileTxt = new FileHandler("Logging.txt");
			fileTxt.setFormatter(new SimpleFormatter());
		    logger.addHandler(fileTxt);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    littlePartition();

		Method m = new Method("M", "M(x)=N(x);H(x);H(x)");
		Method n = new Method("N", "N(x)=O(x)");
		Method o = new Method("O", "O(x)=P(x)");
		Method p = new Method("P", "P(x)=M(x)||F(x)");

		Method h = new Method("H", "H(x)=I(x)");
		Method i = new Method("I", "I(x)=G(x)");
		Method g = new Method("G", "G(x)=H(x);F(x)");


		Method f = new Method("F", "F(x)=E(x)");
		Method e = new Method("E", "E(x)=F(x)");

		Set<Method> allMethods = new HashSet<Method>();
		allMethods.add(m);
		allMethods.add(n);
		allMethods.add(o);
		allMethods.add(p);

		allMethods.add(h);
		allMethods.add(i);
		allMethods.add(g);

		allMethods.add(f);
		allMethods.add(e);

		m.rebuildChildrenList(allMethods);
		n.rebuildChildrenList(allMethods);
		o.rebuildChildrenList(allMethods);
		p.rebuildChildrenList(allMethods);

		h.rebuildChildrenList(allMethods);
		i.rebuildChildrenList(allMethods);
		g.rebuildChildrenList(allMethods);

		f.rebuildChildrenList(allMethods);
		e.rebuildChildrenList(allMethods);

		MethodTree MT = new MethodTree(m);
		Set<Partition<Method>> partitions = MT.getPartitions();

		assertTrue(partitions != null);
		assertTrue(partitions.size() == 3);

		// Non linear

		Method m1 = new Method("M1", "M1(x,y,z)=(x,h)||N1(y,z)||N1(x,z)");
		Method n1 = new Method("N1", "N1(x,y)=M1(x,y,z)||M1(z,y,y);(x,z)");

		m1.addChild(n1);
		m1.addChild(n1);

		n1.addChild(m1);
		n1.addChild(m1);

		MT = new MethodTree(n1);
	}
*/

	@Test
	public void testNonLinear() throws Exception
	{
		Logger logger = Logger.getLogger("");
	    logger.setLevel(Level.FINEST);
	    try {
			FileHandler fileTxt = new FileHandler("Logging.txt");
			fileTxt.setFormatter(new SimpleFormatter());
		    logger.addHandler(fileTxt);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    Method m2 = new Method("M2", "M2(x)=(x,y)||N2(x)");
	    Method n2 = new Method("N2", "N2(x)=(x,y)||M2(x);N2(y)");

		Set<Method> allMethods = new HashSet<Method> ();
		allMethods.add(m2);
		allMethods.add(n2);

		m2.rebuildChildrenList(allMethods);
		n2.rebuildChildrenList(allMethods);

		MethodTree MT = new MethodTree(m2);

		SaturateMethod worker = new SaturateMethod();
		Set<Partition<Method>> part = MT.getPartitions();

		Method ASaturato = worker.saturate(m2, part, MT.getAllMethods());

		CircularitiesFinder cf = new CircularitiesFinder(ASaturato.getAST());
		assertTrue(!cf.hasCirc());
}

/*

	@Test
	public void testNonLinearDue() throws Exception
	{
		Method m = new Method("M", "M(x)=N(x)||N(x)");
		Method n = new Method("N", "N(x)=O(x)||O(x)");
		Method o = new Method("O", "O(x)=M(x)||M(x)");

		Set<Method> allMethods = new HashSet<Method> ();
		allMethods.add(m);
		allMethods.add(n);
		allMethods.add(o);

		m.rebuildChildrenList(allMethods);
		n.rebuildChildrenList(allMethods);
		o.rebuildChildrenList(allMethods);

		MethodTree MT = new MethodTree(m);

		MT.getPartitions();
	}*/
}
