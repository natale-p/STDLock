package edu.unibo.cs.STDLock.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import edu.unibo.cs.STDLock.Method;
import edu.unibo.cs.STDLock.Pair;
import edu.unibo.cs.STDLock.StringContext;
import edu.unibo.cs.STDLock.LAM.node.AFunctionExpr;

public class MethodTest {

	@Test
	public void testGetInstantiationVariables() throws Exception
	{
		Method m = new Method("M", "M(x,y)=N(x,y)");
		Method n = new Method("N", "N(a,b)=M(c,d)");
		Method Q = new Method("Q", "Q(x)=(x,y);M(x,y);N(x,y);N(y,x)");

		Set<Method> allMethods = new HashSet<Method>();
		allMethods.add(m);
		allMethods.add(n);
		allMethods.add(Q);

		m.rebuildChildrenList(allMethods);
		n.rebuildChildrenList(allMethods);
		Q.rebuildChildrenList(allMethods);

		assertTrue(Q.getChildren().size() == 3);

		List<Pair<AFunctionExpr, Method>> mChild = m.getChildren();
		assertEquals(mChild.size(), 1);

		AFunctionExpr invkOfN = mChild.get(0).getLeft();

		Map<String, String> nToM = m.getInstantiationVariables(invkOfN, n);

		assertEquals(nToM.get("a"), "x");
		assertEquals(nToM.get("b"), "y");

		List<Pair<AFunctionExpr, Method>> nChild = n.getChildren();
		assertEquals(nChild.size(), 1);

		AFunctionExpr invkOfM = nChild.get(0).getLeft();

		Map<String, String> MToN = n.getInstantiationVariables(invkOfM, m);

		assertEquals(MToN.get("x"), "c");
		assertEquals(MToN.get("y"), "d");
	}

	@Test
	public void testInstantiate() throws Exception {

		Method m = new Method("M", "M(x,y)=N(z,w)");
		Method n = new Method("N", "N(x,y)=O(x1,x2)||(x,y);(y,x);(x2,y)");
		Method o = new Method("O", "O(x,y)=(x,y)||(w,z)");

		Set<Method> allMethods = new HashSet<Method>();
		allMethods.add(m);
		allMethods.add(n);
		allMethods.add(o);

		m.rebuildChildrenList(allMethods);
		n.rebuildChildrenList(allMethods);
		o.rebuildChildrenList(allMethods);

		StringContext ctx = new StringContext();
		m.initStringContext(ctx);
		Map<String, String> nTOm = new HashMap<String, String>();

		nTOm.put("x", "z");
		nTOm.put("y", "w");

		assertEquals("O(x1,x2)||(x,y);(y,x);(x2,y)", n.ASTToString());

		n.instantiate(nTOm, ctx);

		assertEquals("O(x1,x2)||(z,w);(w,z);(x2,w)", n.ASTToString());
	}

	@Test
	public void testMerge() throws Exception {
		Method m = new Method("M", "M(x,y)=N(x,y);(x1,x2)");
		Method n = new Method("N", "N(x,y)=(x,y)||O(z,w)");
		Method o = new Method("O", "O(x,y)=(x,y);H(x)||H(y)");
		Method h = new Method("H", "H(x)=(x,y)");

		Set<Method> allMethods = new HashSet<Method>();
		allMethods.add(m);
		allMethods.add(n);
		allMethods.add(o);
		allMethods.add(h);

		m.rebuildChildrenList(allMethods);
		n.rebuildChildrenList(allMethods);
		o.rebuildChildrenList(allMethods);
		h.rebuildChildrenList(allMethods);

		StringContext ctx = new StringContext();
		m.initStringContext(ctx);
		Pair<AFunctionExpr, Method> invk = m.getFirstInvocationOf(n);
		Method toMerge = new Method(n);
		m.merge(invk.getLeft(), toMerge, ctx);
		m.mergeChildrenOf(invk, toMerge);

		invk = m.getFirstInvocationOf(o);
		toMerge = new Method(o);
		m.merge(invk.getLeft(), toMerge, ctx);
		m.mergeChildrenOf(invk, toMerge);
		System.out.println(m.ASTToString());
		System.out.println(m.getChildren());
	}

	@Test
	public void testHasTwoOrMoreSameChild() throws Exception {
		Method m = new Method("M", "M(x,y)=N(x,y);N(x1,x2)");
		Method n = new Method("N", "N(a,b)=(a,b)||O(c,d)");

		Set<Method> allMethods = new HashSet<Method>();
		allMethods.add(m);
		allMethods.add(n);

		m.rebuildChildrenList(allMethods);
		n.rebuildChildrenList(allMethods);

		assertTrue(m.hasTwoOrMoreSameChild() != null);
		assertTrue(n.hasTwoOrMoreSameChild() == null);

		assertEquals(m.hasTwoOrMoreSameChild(), n);

		Method m2 = new Method("M2", "M2(x)=(x,y)||N2(x)");
		Method n2 = new Method("N2", "N2(x)=(x,y)||M2(x);N2(y)");

		allMethods = new HashSet<Method> ();
		allMethods.add(m2);
		allMethods.add(n2);

		m2.rebuildChildrenList(allMethods);
		n2.rebuildChildrenList(allMethods);

		assertTrue(m2.hasTwoOrMoreSameChild() == null);
		assertTrue(n2.hasTwoOrMoreSameChild() == null);
	}
/*

	@Test
	public void testGetDefinition() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetChildren() {
		fail("Not yet implemented");
	}

*/
}
