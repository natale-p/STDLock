package edu.unibo.cs.STDLock.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import edu.unibo.cs.STDLock.Method;
import edu.unibo.cs.STDLock.Pair;
import edu.unibo.cs.STDLock.StringContext;
import edu.unibo.cs.STDLock.LAM.node.AFunctionExpr;

public class MethodCoreTest {

	@Test
	public void testMethodStringString() {
		try {
			@SuppressWarnings("unused")
			Method m = new Method("M", "M(x)=0");

			m = new Method("M", "M(x)=(x,y)");

			m = new Method("M", "M(x)=(x,y)");
			m = new Method("M", "M(x)=M(x)");
			m = new Method("M", "M(x)=(x,y)||(x,y)");
			m = new Method("M", "M(x)=M(x)||M(x)");
			m = new Method("M", "M(x)=(x,y)||M(x)");

			m = new Method("M", "M(x)=(x,y);(x,y)");
			m = new Method("M", "M(x)=M(x);M(x)");
			m = new Method("M", "M(x)=(x,y);M(x)");


			m = new Method("M", "M(x)=((x,y)||(x,y))");
			m = new Method("M", "M(x)=(M(x)||M(x))");
			m = new Method("M", "M(x)=((x,y)||M(x))");

			m = new Method("M", "M(x)=(a,b)||(c,d);(e,f)||(g,h)");
		} catch (Exception e) {
			System.out.println("failed: " + e.getMessage());
			fail();
		}

		try {
			Method m = new Method("M", "M(x,y)=M(x,)");
			Set<Method> allMethods = new HashSet<Method>();
			allMethods.add(m);

			m.rebuildChildrenList(allMethods);
			System.out.println(m.getAST());

			m = new Method("M", "M()=(x,y)");

			m.rebuildChildrenList(allMethods);
			System.out.println(m.getAST());

			fail();
		} catch (Exception e) {
		}

		try {
			Method m = new Method("M", "M(x,)=M(x,y)");
			Set<Method> allMethods = new HashSet<Method>();
			allMethods.add(m);

			m.rebuildChildrenList(allMethods);

			System.out.println(m.getAST());
			fail();
		} catch (Exception e) {
		}

		try {
			Method m = new Method("M", "M(x,y)=(x,y)||N(x)");
			Method n = new Method("N", "N(x)=(u,w)");

			Method mCopy = new Method(m);

			StringContext ctx = new StringContext();

			Pair<AFunctionExpr, Method> invk = m.getFirstInvocationOf(n);
			Method toMerge = new Method(n);
			m.merge(invk.getLeft(), toMerge, ctx);

			assertTrue(!m.getAST().equals(mCopy.getAST()));

		} catch (Exception e) {

		}
	}

	@Test
	public void testGetArgsCount() throws Exception {
		Method m = new Method("M", "M(x)=(x,y)");
		Method n = new Method("N", "M(a)=(x,y)");
		Method o = new Method("O", "M(a,b,c)=(x,y)");
		Method p = new Method("P", "P(x,y,z)=(x,y)");

		assertEquals(m.getArgsCount(), 1);
		assertEquals(n.getArgsCount(), 1);
		assertEquals(o.getArgsCount(), 3);
		assertEquals(p.getArgsCount(), 3);
	}

	@Test
	public void testEqualsObject() throws Exception {
		Method m = new Method("M", "M(x)=N(y)");
		Method p = new Method(m);

		assertTrue(m.equals(p));
	}

	@Test
	public void testGetName() {
		try {
			Method m = new Method("M", "M(x)=(x,y)");
			Method n = new Method("N200", "N200(x)=(x,y)");
			Method o = new Method("O2", "O2(x)=(x,y)");
			Method p = new Method("F", "F(x)=(x,y)");

			assertEquals(m.getName(), "M");
			assertEquals(n.getName(), "N200");
			assertEquals(o.getName(), "O2");
			assertEquals(p.getName(), "F");
		} catch (Exception e) {
			System.out.println(e);
			assertTrue(0 == 1);
		}

		try {
			Method m = new Method("299M", "299M(x)=(x,u)");
			assertEquals(m.getName(), "299M");
			assertTrue(0 == 1);
		} catch (Exception e) {
		}
	}

	@Test
	public void testRebuildChildrenList() throws Exception {
		Method m = new Method("M", "M(x)=N(x,y)");
		Method n = new Method("N", "N(x,y)=O(x,y)");
		Method o = new Method("O", "O(a,b)=P(x,y);P(x,y)");
		Method p = new Method("P", "P(x,y)=M(x,y)||P(x,y)");
		Method regr1 = new Method("R1", "R1(x)=N(x);P(x);P(x)");

		Set<Method> allMethods = new HashSet<Method>();
		allMethods.add(m);
		allMethods.add(n);
		allMethods.add(o);
		allMethods.add(p);
		allMethods.add(regr1);

		m.rebuildChildrenList(allMethods);
		n.rebuildChildrenList(allMethods);
		o.rebuildChildrenList(allMethods);
		p.rebuildChildrenList(allMethods);
		regr1.rebuildChildrenList(allMethods);

		assertTrue(m.getBound().size() == 1);
		assertTrue(m.getFree().size() == 1);

		List<Pair<AFunctionExpr, Method>> mChild = m.getChildren();
		assertTrue(mChild.size() == 1);
		for (Pair<AFunctionExpr, Method> pair : mChild) {
			assertEquals(pair.getRight(), n);
		}

		assertTrue(n.getBound().size() == 2);
		assertTrue(n.getFree().size() == 0);

		List<Pair<AFunctionExpr, Method>> nChild = n.getChildren();
		assertTrue(nChild.size() == 1);
		for (Pair<AFunctionExpr, Method> pair : nChild) {
			assertEquals(pair.getRight(), o);
		}


		assertTrue(o.getBound().size() == 0);
		assertTrue(o.getFree().size() == 4);

		List<Pair<AFunctionExpr, Method>> oChild = o.getChildren();
		assertTrue(oChild.size() == 2);
		for (Pair<AFunctionExpr, Method> pair : oChild) {
			assertEquals(pair.getRight(), p);
		}



		List<Pair<AFunctionExpr, Method>> pChild = p.getChildren();
		assertTrue(pChild.size() == 2);
		for (Pair<AFunctionExpr, Method> pair : pChild) {
			assertTrue(pair.getRight().equals(p) || pair.getRight().equals(m));
		}

		List<Pair<AFunctionExpr, Method>> regr1Child = regr1.getChildren();
		assertTrue(regr1Child.size() == 3);
		for (Pair<AFunctionExpr, Method> pair : regr1Child) {
			assertTrue(pair.getRight().equals(p) || pair.getRight().equals(n));
		}
	}

	@Test
	public void testGetFree() throws Exception
	{
		Method m = new Method("M", "M(x)=(x,y)");
		Method n = new Method(m);

		assertTrue(m.getFree().size() == 1);
		assertTrue(n.getFree().size() == 1);
	}


	@Test
	public void testGetChildren() throws Exception
	{
		Method m = new Method("M", "M(x)=N(x,y)||N(x,y)");
		Method n = new Method("N", "N(x,y)=(x,y)");


		Set<Method> allMethods = new HashSet<Method>();
		allMethods.add(m);
		allMethods.add(n);

		m.rebuildChildrenList(allMethods);
		n.rebuildChildrenList(allMethods);
	}

	@Test
	public void testNewMethod() throws Exception
	{
		Method m = new Method("M", "M(x)=M(x,y)||M(x,y)");

		Set<Method> allMethods = new HashSet<Method>();
		allMethods.add(m);

		m.rebuildChildrenList(allMethods);

		Method m1 = new Method(m);

		assertTrue(m1.getChildren().size() == m.getChildren().size());
	}
}
