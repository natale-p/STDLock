package edu.unibo.cs.STDLock.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import edu.unibo.cs.STDLock.CircularitiesFinder;
import edu.unibo.cs.STDLock.CircularitiesFinder.Hen;
import edu.unibo.cs.STDLock.PairOfStrings;
import edu.unibo.cs.STDLock.LAM.node.APairExpr;
import edu.unibo.cs.STDLock.LAM.node.AParallelExpr;
import edu.unibo.cs.STDLock.LAM.node.ASemicolonExpr;
import edu.unibo.cs.STDLock.LAM.node.AZeroExpr;
import edu.unibo.cs.STDLock.LAM.node.PExpr;
import edu.unibo.cs.STDLock.LAM.node.TId;
import edu.unibo.cs.STDLock.LAM.node.TZero;

public class CircularitiesFinderTest {

	private List<PairOfStrings> getState(PairOfStrings ... states)
	{
		List<PairOfStrings> s1 = new ArrayList<PairOfStrings>();

		for (int i=0; i<states.length; i++) {
			s1.add(states[i]);
		}

		return s1;
	}

	@Test
	public void testHen()
	{
		List<List<PairOfStrings>> states = new ArrayList<List<PairOfStrings>>();
		states.add(getState(new PairOfStrings("x", "y"),new PairOfStrings("y", "z"),new PairOfStrings("z", "w"),new PairOfStrings("z", "y")));

		List<Hen> hens = CircularitiesFinder.buildHenFrom(states.get(0));
		assertEquals(hens.toString(), "[(x-->[y]), (y-->[z]), (z-->[w,y]), (w-->[])]");

	}

	private void checkCircMio(List<PairOfStrings> state, boolean hasDead) {
		List<Hen> hens = CircularitiesFinder.buildHenFrom(state);

		boolean c = CircularitiesFinder.checkMio(hens);
		assertEquals(c, hasDead);
	}

	private void checkCircCarlo(List<PairOfStrings> state, boolean hasDead) {
		List<Hen> hens = CircularitiesFinder.buildHenFrom(state);

		boolean c = CircularitiesFinder.checkCarlo(hens);
		assertEquals(c, hasDead);
	}

	@Test
	public void testCarlo()
	{
		List<PairOfStrings> s = (getState(new PairOfStrings("x", "y"),new PairOfStrings("y", "z"),new PairOfStrings("z", "w"),new PairOfStrings("z", "y")));

		checkCircMio(s, true);
		checkCircCarlo(s, true);

		s = getState(new PairOfStrings("x","y"), new PairOfStrings("x", "z"), new PairOfStrings("y", "w"), new PairOfStrings("y", "u"),
				new PairOfStrings("z", "y"));

		checkCircMio(s, false);
		checkCircCarlo(s, false);

		s = getState(new PairOfStrings("w", "x"), new PairOfStrings("w", "y"), new PairOfStrings("y", "z"), new PairOfStrings("y", "w"),
				new PairOfStrings("z", "x"));

		checkCircMio(s, true);
		checkCircCarlo(s, true);

		s = getState(new PairOfStrings("w", "x"), new PairOfStrings("w", "y"), new PairOfStrings("y", "z"),
				new PairOfStrings("z", "x"), new PairOfStrings("x", "y"));

		checkCircMio(s, true);
		checkCircCarlo(s, true);

		// Esempi elena
		s = getState(new PairOfStrings("x", "y"), new PairOfStrings("x", "z"), new PairOfStrings("y", "w"),
				new PairOfStrings("w", "w1"), new PairOfStrings("w1", "z"));
		checkCircMio(s, false);
		checkCircCarlo(s, false);

		s = getState(new PairOfStrings("x", "y"), new PairOfStrings("x", "z"), new PairOfStrings("y", "w"),
				new PairOfStrings("w", "w1"), new PairOfStrings("w1", "z"), new PairOfStrings("z", "w"));
		checkCircMio(s, true);
		checkCircCarlo(s, true);

		// Circ in x
		s = getState(new PairOfStrings("x", "y"), new PairOfStrings("y","x"));
		checkCircMio(s, true);
		checkCircCarlo(s, true);

		s = getState(new PairOfStrings("x", "y"), new PairOfStrings("y","z"), new PairOfStrings("z","x"));
		checkCircMio(s, true);
		checkCircCarlo(s, true);

		// Circ non in x, ma in cui si viene portati da x
		s = getState(new PairOfStrings("x", "y"), new PairOfStrings("y","z"), new PairOfStrings("z","w"),
				new PairOfStrings("w","y"));
		checkCircMio(s, true);
		checkCircCarlo(s, true);

		s = new ArrayList<PairOfStrings>();
		for (int i=0; i<100000; i++) {
			s.add(new PairOfStrings("x"+i,"y"+i));
		}

		List<List<PairOfStrings>> s1 = new ArrayList<List<PairOfStrings>>();
		s1.add(s);
		CircularitiesFinder cf = new CircularitiesFinder(s1);
	}

	private APairExpr getPair(String x, String y)
	{
		APairExpr p1 = new APairExpr();

		p1.setFirst(new TId(x));
		p1.setSecond(new TId(y));

		return p1;
	}

	private AParallelExpr getParallel(PExpr left, PExpr right)
	{
		AParallelExpr p1 = new AParallelExpr();

		p1.setLeft(left);
		p1.setRight(right);

		return p1;
	}

	private ASemicolonExpr getSemicolon(PExpr left, PExpr right)
	{
		ASemicolonExpr p1 = new ASemicolonExpr();

		p1.setLeft(left);
		p1.setRight(right);

		return p1;
	}

	public void check(PExpr node, String res)
	{
		List<List<PairOfStrings>> states = CircularitiesFinder.process(node);
		//System.out.println("STATI: " + states.toString());
		for (List<PairOfStrings> s : states) {
			List<Hen> hens = CircularitiesFinder.buildHenFrom(s);
			//System.out.println(hens);
		}

		assertEquals(states.toString(), res);
	}

	@Test
	public void testProcess()
	{
		ASemicolonExpr e2 = getSemicolon(
				getParallel(getPair("x", "y"), getPair("y", "z")),
				getParallel(getPair("x", "y"), getPair("y", "z")));

		check(e2, "[[(x,y), (y,z)], [(x,y), (y,z)]]");

		check(getParallel(getPair("x", "y"), getPair("y", "z")), "[[(x,y), (y,z)]]");

		AParallelExpr e3 = getParallel(
				getParallel(getPair("x", "y"), getPair("y", "z")),
				getSemicolon(getPair("c", "d"), getPair("d", "e")));

		check(e3, "[[(x,y), (y,z), (c,d)], [(x,y), (y,z), (d,e)]]");

		AParallelExpr e4 = getParallel(e3, getPair("z", "w"));
		check(e4, "[[(x,y), (y,z), (c,d), (z,w)], [(x,y), (y,z), (d,e), (z,w)]]");

		AParallelExpr e5 = getParallel(getPair("z", "w"), new AZeroExpr(new TZero()));
		check(e5, "[[(z,w)]]");

		ASemicolonExpr e6 = getSemicolon(getParallel(getPair("z", "w"), new AZeroExpr(new TZero())),
				getParallel(getPair("z", "w"), new AZeroExpr(new TZero())));
		check(e6, "[[(z,w)], [(z,w)]]");

		AParallelExpr e7 = getParallel(
				getParallel(getPair("x", "y"), new AZeroExpr(new TZero())),
				getSemicolon(getPair("c", "d"), getPair("d", "e")));

		check(e7, "[[(x,y), (c,d)], [(x,y), (d,e)]]");

		AParallelExpr e8 = getParallel(e2, e3);
		check(e8, "[[(x,y), (y,z), (x,y), (y,z), (c,d)], [(x,y), (y,z), (x,y), (y,z), (d,e)], [(x,y), (y,z), (x,y), (y,z), (c,d)], [(x,y), (y,z), (x,y), (y,z), (d,e)]]");
	}
}
