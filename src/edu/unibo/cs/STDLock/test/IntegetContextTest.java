package edu.unibo.cs.STDLock.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;

import edu.unibo.cs.STDLock.IntegerContext;

public class IntegetContextTest {

	@Test
	public void testGetInitialArgs() {
		IntegerContext ctx = new IntegerContext();

		List<Integer> rs1 = ctx.getInitialValues(10);
		List<Integer> rs2 = ctx.getInitialValues(1);

		IntegerContext ctx1 = new IntegerContext();
		List<Integer> rs3 = ctx1.getInitialValues(5);

		IntegerContext ctx2 = new IntegerContext();
		List<Integer> rs4 = ctx2.getInitialValues(-3);

		assertEquals(rs1.size(), 10);
		assertNull(rs2);

		assertEquals(rs3.size(), 5);
		assertEquals(rs4.size(), 0);
	}

	@Test
	public void testGetFreshArgs() {
		IntegerContext ctx = new IntegerContext();

		List<Integer> rs1 = ctx.getInitialValues(10);

		rs1 = ctx.getFreshValues(10);

		assertEquals(rs1.size(), 10);
	}

	@Test
	public void testCompareArgs() {
		fail("Not yet implemented");
	}

}
