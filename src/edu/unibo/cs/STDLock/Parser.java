package edu.unibo.cs.STDLock;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class Parser {

	public static void main(String[] args) throws Exception {
		if (args.length < 1) {
			System.out.println("Welcome to STDLock. Usage:");
			System.out.println("java -classpath $(pwd)/bin/ edu.unibo.cs.STDLock.Parser file.txt");
			System.out.println("\nWhere file.txt contains LAM specification in plain text. See example.txt for an example");
			return;
		}

		Parser p = new Parser(args[0]);
	}
	
	public Parser(String file) {
		Set<Method> allMethods = new HashSet<Method>();
		Method root = null;
		
		try {
			FileInputStream fstream = new FileInputStream(file);
			// Get the object of DataInputStream
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;

			//Read File Line By Line
			int i = 0;
			while ((strLine = br.readLine()) != null)   {
				try {
					Method M;
					if (strLine.length() <= 1 || strLine.trim().length() == 0)
						continue;
					
                    strLine = strLine.replaceAll("\\s","");
                    String firstChar = strLine.substring(0,1);
					String name = strLine.split("\\(")[0];
					
					if (firstChar.equals("/"))
						continue;
			

					M = new Method(name, strLine);
					allMethods.add(M);

					if (i == 0)
						root = M;
					
					i++;
				} catch (Exception e) {
                    System.err.println("Error found when parsing the file:" + e.getMessage()+"\nExiting..");
                    in.close();
                    return;
				}
			}
			//Close the input stream
			in.close();
		} catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage() + "\nExiting..");
            return;
		}
		
		for (Method M : allMethods) {
			M.rebuildChildrenList(allMethods);
		}
		
        long startC = System.nanoTime();
		MethodTree MT = new MethodTree(root);
		Set<Partition<Method>> partitions = MT.getPartitions();
		SaturateMethod worker = new SaturateMethod();

		Method ASaturato = worker.saturate(root, partitions, allMethods);

		CircularitiesFinder cf = new CircularitiesFinder(ASaturato.getAST());
        long endC = System.nanoTime();
        System.out.println("Execution time: " + (endC-startC)/1000000 + "ms");

		if (cf.hasCirc()) {
			System.out.println("### Deadlock exists in the state " + cf.whoCaused);
		} else {
			System.out.println("### No deadlock found");
		}
	}
}
