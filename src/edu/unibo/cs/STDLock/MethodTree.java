package edu.unibo.cs.STDLock;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Logger;

import edu.unibo.cs.STDLock.LAM.node.AFunctionExpr;

/** Tree of method
 *
 * Consider methods as nodes, and arcs relation between caller / called methods.
 * What you get is a method tree, represented by this class.
 *
 * @author danimoth
 *
 */
public class MethodTree {

	private static Logger logger=Logger.getLogger("edu.unibo.cs.STDLock.MethodTree");
	private Set<Partition<Method>> partitions = null;
	private Set<Method> methods = null;
	private final Method root;

	/** Initialize a new method tree, starting at root
	 *
	 * @param root Starting method
	 */
	public MethodTree(Method root) {
		this.root = new Method(root);
		methods = new HashSet<Method>();
	}

	public Set<Method> getAllMethods()
	{
		return methods;
	}

	/** Get the root of this method tree
	 *
	 * @return Root of the tree
	 */
	public Method getRoot() {
		return new Method(this.root);
	}

	@Override
	public boolean equals(Object other) {

		if (this == other)
			return true;

		if (! (other instanceof MethodTree))
			return false;

		MethodTree that = (MethodTree) other;

		return (this.root.getName() == that.getRoot().getName());
	}

	/** Get the partitions of this tree.
	 *
	 * See paper for the algorithm
	 * @return A set of partitions
	 */
	public Set<Partition<Method>> getPartitions() {
		if (partitions != null)
			return partitions;

		methods = new HashSet<Method>();

		Stack<Method>                 S = new Stack<Method>();
		HashMap<Method, List<Pair<AFunctionExpr, Method>>> T = new HashMap<Method, List<Pair<AFunctionExpr, Method>>>();
		Set<Partition<Method>> C = new HashSet<Partition<Method>>();

		boolean isLinear = true;

		S.push(root);
		T.put(root, root.getChildren());

		logger.fine("Rule #1: We have   S=" + S.toString() + " T=" + T.toString());

		while (! S.isEmpty()) {
			Method M = S.peek();
			methods.add(M);

			List<Pair<AFunctionExpr, Method>> childsOfM = T.get(M);

			if (childsOfM.isEmpty()) {
				logger.fine("Rule #4, extracting from S: " + M.toString());
				T.remove(M);
				S.pop();
				continue;
			}

			Method N = childsOfM.get(0).getRight();
			childsOfM.remove(0);

			if (S.contains(N)) {
				// Regola 3: Ciclo!
				List<Method> cycle = new ArrayList<Method>(S.subList(S.indexOf(N), S.size()));

				/* 1. I nodi appena trovati o sono tutti in un altro ciclo o
	                  non ce ne deve essere nemmeno uno che li contiene */

				for (Partition<Method> partition : C) {
					Partition<Method> setP = new Partition<Method>(partition);
					setP.retainAll(cycle);

					if (setP.size() > 0 && setP.size() != cycle.size()) {
						logger.warning("It isn't linear! Two (or more) cycles which have same nodes");
						logger.warning(partition.toString() + cycle.toString());

						isLinear = false;

						//return null;
					}
				}

				/* 2. I nodi appena trovati non devono essere nè tra i figli di M, di N o
		              di qualsiasi altro nodo all'interno del ciclo */

				for (Method cycleNode : cycle) {
					if (childsOfM.contains(cycleNode)) {
						logger.warning("Is isn't linear!");
						isLinear = false;
						//return null;
					}

					for (Method other_node : cycle) {
						Collection<Pair<AFunctionExpr, Method>> childOfOther = T.get(other_node);
						for (Pair<AFunctionExpr, Method> p : childOfOther) {
							if (p.getRight().equals(cycleNode)) {
								logger.warning("It isn't linear!");
								isLinear = false;
								break;
								//return null;
							}
						}
					}
				}

				Partition<Method> c = new Partition<Method>(cycle);

				for (Method cycleNode : cycle) {
					List<Method> newCycle = new ArrayList<Method>();
					newCycle.addAll(cycle);
					newCycle.remove(newCycle.indexOf(cycleNode));
					cycleNode.addHistory(newCycle);
					cycleNode.setPartition(c);
				}

				C.add(c);

				logger.fine("Rule #3: M = " + M.toString() + " N=" + N.toString() +
						" cycle="+cycle.toString());
			} else {
				// Regola 2: Avanzo nella DFS
				S.add(N);
				T.put(N, N.getChildren());

				logger.fine("Rule #2: M=" + M.toString() + " N=" + N.toString());
			}
		}

		if (isLinear) {
			this.partitions = C;
			return C;
		} else {
			logger.fine("The program isn't linear. Checking if it is pseudolinear..");

			boolean isPseudoLinear = true;
			for (Partition<Method> partition : C) {
				int bigRecursiveHistoryCount = 0;
				for (Method m : partition) {
					if (m.getHistory().size() > 1) {
						logger.fine("Method " + m + " has " + m.getHistory().size() + " recursive histories");
						bigRecursiveHistoryCount++;
					}
				}

				if (bigRecursiveHistoryCount > 1) {
					isPseudoLinear = false;
					break;
				}
			}

			if (isPseudoLinear) {
				return convertFromPseudoLinearAndGetPartitions();
			} else
				return convertFromGeneralCaseAndGetPartitions(C);
		}

	}

	private boolean checkIfHistoriesContainsMethod(List<List<Method>> histories, Method M)
	{
		for (List<Method> history : histories) {
			if (history.contains(M))
				return true;
		}

		return false;
	}

	private Set<Partition<Method>> convertFromPseudoLinearAndGetPartitions()
	{
		boolean a = false;

		for (Method m : methods) {
			Method twoTimeChild = m.hasTwoOrMoreSameChild();

			if (m.getHistory().size() > 1) {

				System.out.println("Applying 1st rule on method " + m.getDefinition() + "...");

				List<Pair<AFunctionExpr, Method>> childrenOfM = m.getChildren();
				List<List<Method>> historiesOfM = m.getHistory();

				for (Pair<AFunctionExpr, Method> p : childrenOfM) {
					Method realChild = p.getRight();

					if (! m.equals(realChild) && checkIfHistoriesContainsMethod(historiesOfM, realChild)) {
						StringContext ctx = new StringContext();
						m.initStringContext(ctx);
						Pair<AFunctionExpr, Method> invk = m.getFirstInvocationOf(realChild);
						Method toMerge = new Method(realChild);
						m.merge(invk.getLeft(), toMerge, ctx);
						m.mergeChildrenOf(invk, toMerge);

						m.resetHistory();
						break;
					}
				}

					System.out.println("Done! We have: " + m.getDefinition());
					a = true;
					break;
			}

			if (twoTimeChild != null) {
				a = true;
				//toReplaceInstance.put(m, new Method(twoTimeChild));
				System.out.println("Applying 2nd rule on method " + m.getDefinition() + "...");
				StringContext ctx = new StringContext();
				m.initStringContext(ctx);

				Pair<AFunctionExpr, Method> invk = m.getFirstInvocationOf(twoTimeChild);
				Method toMerge = new Method(twoTimeChild);
				m.merge(invk.getLeft(), toMerge, ctx);
				m.mergeChildrenOf(invk, toMerge);
				m.resetHistory();

				System.out.println("Done! We have: " + m.getDefinition());
				break;
			}

			if (m.hasTwoOrMoreSelfChild()) {
				a = true;
				int sizeOfMethods = methods.size();
				System.out.println("Applying 3rd rule on method " + m.getDefinition() + "...");
				Method MAux = m.doThirdTransformation(methods);
				assert(sizeOfMethods+1 == methods.size());

				System.out.println("Done! We have: " + m.getDefinition() + "\n" + MAux.getDefinition());
				break;
			}
		}

		if (a == false) {
			System.out.println("BUG. No transformation done, but we're pseudolinear.. exiting");
			System.exit(1);
		}
		logger.fine("System is now in this form:");
		for (Method m : methods) {
			logger.fine(m.getDefinition());
		}

		return getPartitions();
	}

	private Set<Partition<Method>> convertFromGeneralCaseAndGetPartitions(Set<Partition<Method>> partitions)
	{
		System.out.println("General case not implemented, methods are:");

		for (Method m : methods) {
			System.out.println(m.getDefinition());
		}

		return null;
	}
}
